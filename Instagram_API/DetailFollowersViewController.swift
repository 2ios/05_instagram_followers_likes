//
//  DetailFollowersViewController.swift
//  Instagram_API
//
//  Created by Andrei Golban on 01.08.17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import UIKit

class DetailFollowersViewController: UIViewController {
    
    //MARK: Variables
    var username: String = ""
    var image = UIImage()
    var name: String = ""
    var followers: Int64 = 0
    var following: Int64 = 0
    var posts: Int64 = 0
    
    //MARK: IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var followingLabel: UILabel!
    @IBOutlet weak var postsLabel: UILabel!
    @IBOutlet weak var background: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.getInfoByName()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Functions for requests.
    func getInfoByName() {
        Swift_InstagramSDK.sharedInstance.getInfoByName(username: username) { (dict, error) in
            if error == nil {
                let user = dict!.object(forKey: "user") as! NSDictionary
                let photoUrl = user.object(forKey: "profile_pic_url") as! String
                let fullName = user.object(forKey: "full_name") as! String
                let followersCount = user.object(forKey: "follower_count") as! Int64
                let followingCount = user.object(forKey: "following_count") as! Int64
                let postCount = user.object(forKey: "media_count") as! Int64
                self.imageView.sd_setImage(with: URL(string: photoUrl))
                self.nameLabel.text = fullName
                self.followersLabel.text = String(describing: followersCount)
                self.followingLabel.text = String(describing: followingCount)
                self.postsLabel.text = String(describing: postCount)
            } else {
                print(error!.localizedDescription)
            }
            DispatchQueue.main.async {
                print("GET Info is completed")
            }
        }
    }
    
    deinit {
        print("Deinit DetailFollowersViewController ok")
    }
}
