//
//  MenuViewController.swift
//  Menu
//
//  Created by Andrei Golban on 7/29/17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import UIKit
import GoogleMobileAds

class MenuViewController: UITableViewController, GADInterstitialDelegate {
    
    //MARK: Variables
    lazy var menuItems: NSArray = {
        let path = Bundle.main.path(forResource: "MenuItems", ofType: "plist")
        return NSArray(contentsOfFile: path!)!
    }()
    
    var interstitial: GADInterstitial!
    
    var countOfBanner = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Remove the drop shadow from the navigation bar
        navigationController!.navigationBar.clipsToBounds = true
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        StoreManager.shared.setup()
        
    }
    
    func createInterstitial() {
        interstitial = GADInterstitial(adUnitID: keyGoogleAds)
        interstitial.delegate = self
        let request = GADRequest()
//        request.testDevices = [ kGADSimulatorID, "2077ef9a63d2b398840261c8221a0c9b" ]
        interstitial.load(request)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.backgroundView = UIImageView(image: #imageLiteral(resourceName: "GradientVertical"))
        self.createInterstitial()
        if UserDefaults.standard.value(forKey: "countOfBanner") != nil {
            countOfBanner = UserDefaults.standard.value(forKey: "countOfBanner") as! Int
        } else {
            countOfBanner = 0
            UserDefaults.standard.set(countOfBanner, forKey: "countOfBanner")
        }
        UserDefaults.standard.synchronize()
    }
    
    // MARK: UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        
        if indexPath.row < 5 {
            (navigationController!.parent as! ContainerViewController).followersViewController!.navigationController!.tabBarController?.selectedIndex = indexPath.row
        }
        (navigationController!.parent as! ContainerViewController).hideOrShowMenu(show: false, animated: true)
        countOfBanner += 1
        UserDefaults.standard.set(countOfBanner, forKey: "countOfBanner")
        UserDefaults.standard.synchronize()
        
        if StoreManager.shared.isPurchased(id: removeGoogleAds) {
            countOfBanner = 0
            UserDefaults.standard.set(countOfBanner, forKey: "countOfBanner")
            UserDefaults.standard.synchronize()
        } else {
            if countOfBanner == 6 {
                boolForAds = false
                check()
                countOfBanner = 0
                UserDefaults.standard.set(countOfBanner, forKey: "countOfBanner")
                UserDefaults.standard.synchronize()
            } else {
                boolForAds = true
            }
        }
    }
    
    func check() {
        var topVC = self.getCurrentViewController(self)!
        if topVC.isKind(of: UIAlertController.self) {
            topVC.dismiss(animated: true, completion: {
                topVC = self.getCurrentViewController(self)!
                self.interstitial.present(fromRootViewController: self)
            })
        } else {
            self.interstitial.present(fromRootViewController: self)
        }
    }
    
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("OK incarcat")
    }
    
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("Dismiss ok")
        boolForAds = true
    }
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.menuItems.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        return max(80, view.bounds.height / CGFloat(menuItems.count))
        return self.view.bounds.width
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuItemCell") as! MenuItemCell
        let menuItem = menuItems[indexPath.row] as! NSDictionary
        cell.configureForMenuItem(menuItem: menuItem)
        return cell
    }
}
