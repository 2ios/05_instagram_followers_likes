//
//  LikesViewController.swift
//  Instagram_API
//
//  Created by Andrei Golban on 03.08.17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import UIKit
import SDWebImage
import GoogleMobileAds

class LikesViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout, UIPopoverPresentationControllerDelegate, GADInterstitialDelegate {
    
    //MARK: Variables
    var alert: UIAlertController?
    var username: String = ""
    var mediaIds = [String]()
    var users: [NSMutableDictionary] = []
    var numOfMedia = 0
    var mediaNextId: String?
    var likersNextId: String?
    var numberOfCell = 0
    var buffer = 0
    var purchased = false
    
    var hamburgerView: HamburgerView?
    
    var interstitial: GADInterstitial!
    
    var countOfBanner = 0
    
    var checkSelfController = false
    
    //MARK: IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var adsView: UIView!
    @IBOutlet weak var adsImage: UIImageView!
    @IBOutlet weak var unlockBtn: UIButton!
    
    @IBAction func unlockBtn(_ sender: Any) {
        goToBuy()
    }
    
    @IBAction func restoreBtn(_ sender: Any) {
        showWaiting(completion: {
            StoreManager.shared.restoreAllPurchases()
        })
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Remove the drop shadow from the navigation bar
        navigationController!.navigationBar.clipsToBounds = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hamburgerViewTapped))
        hamburgerView = HamburgerView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        hamburgerView!.addGestureRecognizer(tapGestureRecognizer)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: hamburgerView!)
        
        (navigationController!.tabBarController!.parent as! ContainerViewController).likesViewController = self
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        self.username = UserDefaults.standard.object(forKey: "curentUser") as! String
        
        navigationItem.title = "Likes"
        
        addGesture()
        
        NotificationCenter.default.addObserver(self, selector: #selector(catchNotification(notification:)), name: NSNotification.Name(rawValue: "CatchNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkProduct), name: NSNotification.Name(rawValue: "CheckProduct"), object: nil)
    }
    
    func addGesture() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(goToBuy))
        adsView.addGestureRecognizer(gesture)
        adsView.isUserInteractionEnabled = true
    }
    
    func goToBuy() {
        showWaiting {
            if StoreManager.shared.productsFromStore.count > 0 {
                let product = StoreManager.shared.productsFromStore[0]
                StoreManager.shared.buy(product: product)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.collectionView.backgroundView = UIImageView(image: #imageLiteral(resourceName: "GradientBackground"))
        self.createInterstitial()
        
        if UserDefaults.standard.value(forKey: "countOfBanner") != nil {
            countOfBanner = UserDefaults.standard.value(forKey: "countOfBanner") as! Int
        } else {
            countOfBanner = 0
            UserDefaults.standard.set(countOfBanner, forKey: "countOfBanner")
        }
        UserDefaults.standard.synchronize()
    
        if checkSelfController {
            
        } else {
            self.checkProduct()
            self.mediaNextId = ""
            self.likersNextId = ""
            self.numberOfCell = 0
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if checkSelfController {
            checkSelfController = false
        } else {
            if boolForAds {
                showWaiting {
                    self.getFeedMedia()
                }
            }
        }
    }
    
    
    func createInterstitial() {
        interstitial = GADInterstitial(adUnitID: keyGoogleAds)
        interstitial.delegate = self
        let request = GADRequest()
        //        request.testDevices = [ kGADSimulatorID, "2077ef9a63d2b398840261c8221a0c9b" ]
        interstitial.load(request)
    }
    
    func check() {
        var topVC = self.getCurrentViewController(self)!
        if topVC.isKind(of: UIAlertController.self) {
            topVC.dismiss(animated: true, completion: {
                topVC = self.getCurrentViewController(self)!
                self.interstitial.present(fromRootViewController: self)
            })
        } else {
            self.interstitial.present(fromRootViewController: self)
        }
    }
    
    func checkPing() {
        if StoreFinder.userRatedApp() {
            self.adsImage.image = UIImage(named: "2@2x")
        } else {
            self.adsImage.image = UIImage(named: "test")
        }
    }
    
    func checkProduct() {
        if StoreManager.shared.isPurchased(id: nonconsumable1) {
            adsView.isHidden = true
            self.collectionView.isUserInteractionEnabled = true
            purchased = true
            self.collectionView.reloadData()
        } else {
            checkPing()
            
            UIView.animate(withDuration: 1.0, delay: 1.0, options: [.repeat, .autoreverse, .allowUserInteraction], animations: {
                self.unlockBtn.alpha = 0.3
            }, completion: { (success) in
                if !success {
                    self.unlockBtn.alpha = 1.0
                }
            })
            
            adsView.isHidden = false
            self.collectionView.isUserInteractionEnabled = false
            purchased = false
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
        self.mediaIds = []
        self.users = []
        self.collectionView.reloadData()
    }
    
    func hamburgerViewTapped() {
        let navigationController = parent as! UINavigationController
        let containerViewController = navigationController.tabBarController!.parent as! ContainerViewController
        containerViewController.hideOrShowMenu(show: !containerViewController.showingMenu, animated: true)
    }
    
    // MARK: CollectionView Data Source
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.numberOfCell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let lastRowIndex = collectionView.numberOfItems(inSection: 0) - 1
        if indexPath.item == lastRowIndex {
            if (self.numberOfCell + 50) <= self.users.count {
                self.numberOfCell += 50
            } else {
                self.numberOfCell += self.users.count - self.numberOfCell
            }
            if buffer != self.numberOfCell {
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
                buffer = self.numberOfCell
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var n = 0
        if UI_USER_INTERFACE_IDIOM() == .pad {
            n = 6
        } else {
            n = 3
        }
        let width = self.collectionView.frame.width - CGFloat((n-1)*10)
        let widthCell = width / CGFloat(n)
        let heightCell = widthCell
        return CGSize(width: widthCell, height: heightCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LikesCollectionView", for: indexPath) as! LikesCollectionViewCell
        for subview in cell.imageView.subviews {
            if subview is APCustomBlurView {
                subview.removeFromSuperview()
            }
        }
        let photoUrl = self.users[indexPath.row].object(forKey: "photoUrl") as! String
        let url = URL(string: photoUrl)
        cell.imageView.sd_setImage(with: url)
        if !purchased {
            let blurView = APCustomBlurView(withRadius: 3)
            blurView.frame = cell.bounds
            cell.imageView.addSubview(blurView)
        }
        
        cell.labelName.text = self.users[indexPath.row].object(forKey: "fullName") as? String
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        countOfBanner += 1
        UserDefaults.standard.set(countOfBanner, forKey: "countOfBanner")
        UserDefaults.standard.synchronize()
        
        if StoreManager.shared.isPurchased(id: removeGoogleAds) {
            countOfBanner = 0
            UserDefaults.standard.set(countOfBanner, forKey: "countOfBanner")
            UserDefaults.standard.synchronize()
        } else {
            if countOfBanner == 6 {
                boolForAds = true
                check()
                checkSelfController = true
                countOfBanner = 0
                UserDefaults.standard.set(countOfBanner, forKey: "countOfBanner")
                UserDefaults.standard.synchronize()
            } else {
                boolForAds = false
            }
        }
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailLikesViewController")
        
        let detailVC = vc as! DetailLikesViewController
        detailVC.username = self.users[indexPath.row].object(forKey: "userName") as! String
        
        vc.modalPresentationStyle = .popover
        let popover = vc.popoverPresentationController!
        popover.delegate = self
        popover.sourceView = collectionView.cellForItem(at: indexPath)?.contentView
        popover.sourceRect = (collectionView.cellForItem(at: indexPath)?.bounds)!
        present(vc, animated: true, completion:nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    // MARK: Functions for requests.
    func getFeedMedia() {
        Swift_InstagramSDK.sharedInstance.getFeedMedia(username: username, max_next_id: mediaNextId!) { (dict, error) in
            if error == nil {
                let feedMedia = dict!.object(forKey: "items") as! NSArray
                for i in 0..<feedMedia.count {
                    let media = feedMedia[i] as! NSDictionary
                    let media_id = media.object(forKey: "id") as! String
                    self.mediaIds.append(media_id)
                }
                if dict!.object(forKey: "next_max_id") != nil {
                    self.mediaNextId = dict!.object(forKey: "next_max_id") as? String
                } else {
                    self.mediaNextId = nil
                }
            } else {
                print(error!.localizedDescription)
            }
            DispatchQueue.main.async {
                if self.mediaNextId != nil {
                    self.getFeedMedia()
                } else {
                    if self.mediaIds.count > 0 {
                        self.getLikers()
                    }
                }
                print("GET FeedMedia is completed")
            }
        }
    }
    
    func getLikers() {
        let media = mediaIds[numOfMedia]
        Swift_InstagramSDK.sharedInstance.getLikers(media_id: media, username: self.username, next_id: likersNextId!, completion: { (dict, error) in
            if error == nil {
                let likers = dict!.object(forKey: "users") as! NSArray
                for i in 0..<likers.count {
                    let dictionary = NSMutableDictionary()
                    let user = likers[i] as! NSDictionary
                    let fullName = user.object(forKey: "full_name") as! String
                    let photoUrl = user.object(forKey: "profile_pic_url") as! String
                    let userName = user.object(forKey: "username") as! String
                    var count = 0
                    if self.users.count > 0 {
                        for i in 0..<self.users.count {
                            if userName == self.users[i].object(forKey: "userName") as! String {
                                count += 1
                            }
                        }
                    }
                    if count == 0 {
                        dictionary.setObject(fullName, forKey: "fullName" as NSCopying)
                        dictionary.setObject(photoUrl, forKey: "photoUrl" as NSCopying)
                        dictionary.setObject(userName, forKey: "userName" as NSCopying)
                        self.users.append(dictionary)
                    }
                }
                self.numOfMedia += 1
            } else {
                print(error!.localizedDescription)
            }
            DispatchQueue.main.async {
                if self.numOfMedia < self.mediaIds.count {
                    self.getLikers()
                } else {
                    if self.users.count < 50 {
                        self.numberOfCell = self.users.count
                    } else {
                        self.numberOfCell = 50
                    }
                    self.collectionView.reloadData()
                    self.numOfMedia = 0
                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                                    object: nil,
                                                    userInfo: ["title" : "nil", "message" : "Hide"])
                    print("Get Likers is completed")
                }
            }
        })
    }
    
    // MARK: Show waiting spinner
    func showWaiting(completion: (() -> Void)? = nil) -> Void {
        if alert == nil {
            alert = UIAlertController(title: "", message: "Please wait\n\n\n", preferredStyle: .alert)
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            spinner.center = CGPoint(x: 135.0, y: 65.5)
            spinner.color = #colorLiteral(red: 0.8689488173, green: 0.5551353097, blue: 0.5559484959, alpha: 1)
            spinner.startAnimating()
            alert?.view.addSubview(spinner)
        }
        let topVC = self.getCurrentViewController(self)!
        topVC.present(alert!, animated: true, completion: completion)
    }
    
    deinit {
        print("Deinit LikesViewController ok")
    }
}
