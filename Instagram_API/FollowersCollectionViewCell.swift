//
//  FollowersCollectionViewCell.swift
//  Instagram_API
//
//  Created by Andrei Golban on 29.07.17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import UIKit

class FollowersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
}
