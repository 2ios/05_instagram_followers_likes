//
//  HamburgerView.swift
//  Instagram_API
//
//  Created by Andrei Golban on 20.07.17.
//  Copyright (c) 2017 Andrei Golban. All rights reserved.
//

import UIKit

class HamburgerView: UIView {
    
    let imageView: UIImageView! = UIImageView(image: UIImage(named: "Hamburger"))
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        configure()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    // MARK: RotatingView
    func rotate(fraction: CGFloat) {
        let angle = Double(fraction) * .pi/2
        imageView.transform = CGAffineTransform(rotationAngle: CGFloat(angle))
    }
    
    // MARK: Private
    private func configure() {
        imageView.contentMode = UIViewContentMode.center
        addSubview(imageView)
    }
    
}
