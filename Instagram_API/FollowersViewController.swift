//
//  FollowersViewController.swift
//  Instagram_API
//
//  Created by Andrei Golban on 20.07.17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import UIKit
import SDWebImage
import GoogleMobileAds

class FollowersViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIPopoverPresentationControllerDelegate, UICollectionViewDelegateFlowLayout, GADInterstitialDelegate {
    
    //MARK: Variables
    var alert: UIAlertController?
    var username: String = ""
    var followersNextId: String?
    var followersUsers: [NSMutableDictionary] = []
    var boolStopGet = false
    var first = true
    var purchased = false
    
    var hamburgerView: HamburgerView?
    
    var interstitial: GADInterstitial!
    
    var countOfBanner = 0
    
    var checkSelfController = false
    
    //MARK: IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var adsView: UIView!
    @IBOutlet weak var adsImage: UIImageView!
    @IBOutlet weak var unlockBtn: UIButton!
    
    
    @IBAction func unlockBtn(_ sender: Any) {
        goToBuy()
    }
    
    @IBAction func restore(_ sender: Any) {
        showWaiting(completion: {
            StoreManager.shared.restoreAllPurchases()
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Remove the drop shadow from the navigation bar
        navigationController!.navigationBar.clipsToBounds = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hamburgerViewTapped))
        hamburgerView = HamburgerView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        hamburgerView!.addGestureRecognizer(tapGestureRecognizer)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: hamburgerView!)
        
        (navigationController!.tabBarController!.parent as! ContainerViewController).followersViewController = self
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        navigationItem.title = "Followers"
        
        self.username = UserDefaults.standard.object(forKey: "curentUser") as! String
        
        addGesture()
        
        NotificationCenter.default.addObserver(self, selector: #selector(catchNotification(notification:)), name: NSNotification.Name(rawValue: "CatchNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.checkProduct), name: NSNotification.Name(rawValue: "CheckProduct"), object: nil)
        
    }
    
    func addGesture() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(goToBuy))
        adsView.addGestureRecognizer(gesture)
        adsView.isUserInteractionEnabled = true
    }
    
    func goToBuy() {
        showWaiting {
            if StoreManager.shared.productsFromStore.count > 0 {
                let product = StoreManager.shared.productsFromStore[0]
                StoreManager.shared.buy(product: product)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.first = true
        self.collectionView.backgroundView = UIImageView(image: #imageLiteral(resourceName: "GradientBackground"))
        self.createInterstitial()
        if UserDefaults.standard.value(forKey: "countOfBanner") != nil {
            countOfBanner = UserDefaults.standard.value(forKey: "countOfBanner") as! Int
        } else {
            countOfBanner = 0
            UserDefaults.standard.set(countOfBanner, forKey: "countOfBanner")
        }
        UserDefaults.standard.synchronize()
        
        if checkSelfController {

        } else {
            self.checkProduct()
            self.followersNextId = ""
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if checkSelfController {
            checkSelfController = false
        } else {
            if boolForAds {
                showWaiting {
                    self.getFollowers()
                }
            }
        }
    }
    
    func createInterstitial() {
        interstitial = GADInterstitial(adUnitID: keyGoogleAds)
        interstitial.delegate = self
        let request = GADRequest()
        //        request.testDevices = [ kGADSimulatorID, "2077ef9a63d2b398840261c8221a0c9b" ]
        interstitial.load(request)
    }
    
    func check() {
        var topVC = self.getCurrentViewController(self)!
        if topVC.isKind(of: UIAlertController.self) {
            topVC.dismiss(animated: true, completion: {
                topVC = self.getCurrentViewController(self)!
                self.interstitial.present(fromRootViewController: self)
            })
        } else {
            self.interstitial.present(fromRootViewController: self)
        }
    }
    
    func checkPing() {
        if StoreFinder.userRatedApp() {
            self.adsImage.image = UIImage(named: "2@2x")
        } else {
            self.adsImage.image = UIImage(named: "test")
        }
    }
    
    
    func checkProduct() {
        if StoreManager.shared.isPurchased(id: nonconsumable1) {
            self.adsView.isHidden = true
            self.collectionView.isUserInteractionEnabled = true
            purchased = true
            self.collectionView.reloadData()
        } else {
            checkPing()
            
            UIView.animate(withDuration: 1.0, delay: 1.0, options: [.repeat, .autoreverse, .allowUserInteraction], animations: {
                self.adsView.bringSubview(toFront: self.unlockBtn)
                self.unlockBtn.alpha = 0.3
            }, completion: { (success) in
                if !success {
                    self.unlockBtn.alpha = 1.0
                }
            })
            self.adsView.isHidden = false
            self.collectionView.isUserInteractionEnabled = false
            purchased = false
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
        
        if checkSelfController {
            
        } else {
            self.followersUsers = []
            self.collectionView.reloadData()
        }
    }
    
    func hamburgerViewTapped() {
        let navigationController = parent as! UINavigationController
        let containerViewController = navigationController.tabBarController!.parent as! ContainerViewController
        containerViewController.hideOrShowMenu(show: !containerViewController.showingMenu, animated: true)
    }
    
    // MARK: CollectionView Data Source
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.followersUsers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FollowersCollectionView", for: indexPath) as! FollowersCollectionViewCell
        for subview in cell.imageView.subviews {
            if subview is APCustomBlurView {
                subview.removeFromSuperview()
            }
        }
        let photoUrl = self.followersUsers[indexPath.row].object(forKey: "photoUrl") as! String
        let url = URL(string: photoUrl)
        cell.imageView.sd_setImage(with: url)
        
        if !purchased {
            let blurView = APCustomBlurView(withRadius: 3)
            blurView.frame = cell.bounds
            cell.imageView.addSubview(blurView)
        }
        cell.labelName.text = self.followersUsers[indexPath.row].object(forKey: "fullName") as? String
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let lastRowIndex = collectionView.numberOfItems(inSection: 0) - 1
        if indexPath.item == lastRowIndex && !boolStopGet && self.followersNextId != nil {
            DispatchQueue.main.async{
                self.getFollowers()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        countOfBanner += 1
        UserDefaults.standard.set(countOfBanner, forKey: "countOfBanner")
        UserDefaults.standard.synchronize()
        
        if StoreManager.shared.isPurchased(id: removeGoogleAds) {
            countOfBanner = 0
            UserDefaults.standard.set(countOfBanner, forKey: "countOfBanner")
            UserDefaults.standard.synchronize()
        } else {
            if countOfBanner == 6 {
                boolForAds = true
                checkSelfController = true
                check()
                countOfBanner = 0
                UserDefaults.standard.set(countOfBanner, forKey: "countOfBanner")
                UserDefaults.standard.synchronize()
            } else {
                boolForAds = false
            }
        }
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "DetailFollowersViewController")
        
        let detailVC = vc as! DetailFollowersViewController
        detailVC.username = self.followersUsers[indexPath.row].object(forKey: "userName") as! String
        
        vc.modalPresentationStyle = .popover
        let popover = vc.popoverPresentationController!
        popover.delegate = self
        popover.sourceView = collectionView.cellForItem(at: indexPath)?.contentView
        popover.sourceRect = (collectionView.cellForItem(at: indexPath)?.bounds)!
        present(vc, animated: true, completion:nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var n = 0
        if UI_USER_INTERFACE_IDIOM() == .pad {
            n = 6
        } else {
            n = 3
        }
        let width = self.collectionView.frame.width - CGFloat((n-1)*10)
        let widthCell = width / CGFloat(n)
        let heightCell = widthCell
        return CGSize(width: widthCell, height: heightCell)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    // MARK: Functions for requests.
    func getFollowers() {
        Swift_InstagramSDK.sharedInstance.getFollowers(username: username, next_id: followersNextId!) { (dict, error) in
            if error == nil {
                let users = dict!.object(forKey: "users") as! NSArray
                for i in 0..<users.count{
                    let dictionary = NSMutableDictionary()
                    let user = users[i] as! NSDictionary
                    let fullName = user["full_name"] as! String
                    let id = user["pk"] as! Int64
                    let photoUrl = user["profile_pic_url"] as! String
                    let username = user["username"] as! String
                    dictionary.setObject(fullName, forKey: "fullName" as NSCopying)
                    dictionary.setObject(username, forKey: "userName" as NSCopying)
                    dictionary.setObject(id, forKey: "id" as NSCopying)
                    dictionary.setObject(photoUrl, forKey: "photoUrl" as NSCopying)
                    self.followersUsers.append(dictionary)
                }
                if dict!.object(forKey: "next_max_id") != nil {
                    self.followersNextId = dict!.object(forKey: "next_max_id") as? String
                    self.boolStopGet = false
                } else {
                    self.followersNextId = nil
                    self.boolStopGet = true
                }
            } else {
                print(error!.localizedDescription)
            }
            DispatchQueue.main.async {
                self.collectionView.reloadData()
                if self.first {
                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                                    object: nil,
                                                    userInfo: ["title" : "nil", "message" : "Hide"])
                    self.first = false
                }
                print("GET Followers is completed")
            }
        }
    }
    
    // MARK: Show waiting spinner
    func showWaiting(completion: (() -> Void)? = nil) -> Void {
        if alert == nil {
            alert = UIAlertController(title: "", message: "Please wait\n\n\n", preferredStyle: .alert)
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            spinner.center = CGPoint(x: 135.0, y: 65.5)
            spinner.color = #colorLiteral(red: 0.8689488173, green: 0.5551353097, blue: 0.5559484959, alpha: 1)
            spinner.startAnimating()
            alert?.view.addSubview(spinner)
        }
        let topVC = self.getCurrentViewController(self)!
        topVC.present(alert!, animated: true, completion: completion)
    }
    
    deinit {
        print("Deinit FollowersViewController ok")
    }
}
