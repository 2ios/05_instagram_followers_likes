//
//  Extensions.swift
//  Instagram_API
//
//  Created by Andrei Golban on 28.07.17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func getCurrentViewController(_ vc: UIViewController) -> UIViewController? {
        if let pvc = vc.presentedViewController {
            return getCurrentViewController(pvc)
        }
        else if let svc = vc as? UISplitViewController, svc.viewControllers.count > 0 {
            return getCurrentViewController(svc.viewControllers.last!)
        }
        else if let nc = vc as? UINavigationController, nc.viewControllers.count > 0 {
            return getCurrentViewController(nc.topViewController!)
        }
        else if let tbc = vc as? UITabBarController {
            if let svc = tbc.selectedViewController {
                return getCurrentViewController(svc)
            }
        }
        return vc
    }
    
    func catchNotification(notification:Notification) -> Void {
        guard let userInfo = notification.userInfo,
            let title = userInfo["title"] as? String,
            let message  = userInfo["message"] as? String else {
                print("No userInfo found in notification")
                return
        }
        var topVC = self.getCurrentViewController(self)!
        if topVC.isKind(of: UIAlertController.self) {
            topVC.dismiss(animated: true, completion: {
                let alert = UIAlertController(title: "\(title)",
                    message:"\(message)",
                    preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                topVC = self.getCurrentViewController(self)!
                if title != "nil" {
                    topVC.present(alert, animated: true, completion: nil)
                } else if message == "Hide" {
                    print("Hide waiting ok")
                } else {
                    self.performSegue(withIdentifier: message, sender: nil)
                }
            })
        }
    }
}

extension UIColor {
    
    convenience init(colorArray array: NSArray) {
        let r = array[0] as! CGFloat
        let g = array[1] as! CGFloat
        let b = array[2] as! CGFloat
        self.init(red: r/255.0, green: g/255.0, blue: b/255.0, alpha:1.0)
    }
}


