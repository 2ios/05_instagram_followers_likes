//
//  SettingsViewController.swift
//  Instagram_API
//
//  Created by User543 on 10.08.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //MARK: Variables
    
    var hamburgerView: HamburgerView?
    let arrayText = ["Unlock followers & likes content","Remove Ads Forever!","Restore unlocks content"]
    let arrayImage = [#imageLiteral(resourceName: "Unlock"), #imageLiteral(resourceName: "NoAds"),#imageLiteral(resourceName: "Restore")]
    var alert: UIAlertController?
    
    //MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Remove the drop shadow from the navigation bar
        navigationController!.navigationBar.clipsToBounds = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hamburgerViewTapped))
        hamburgerView = HamburgerView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        hamburgerView!.addGestureRecognizer(tapGestureRecognizer)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: hamburgerView!)
        
        (navigationController!.tabBarController!.parent as! ContainerViewController).settingsViewController = self
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.catchNotification(notification:)), name: NSNotification.Name(rawValue: "CatchNotification"), object: nil)
        
        navigationItem.title = "Settings"
        
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.backgroundView = UIImageView(image: #imageLiteral(resourceName: "GradientBackground"))
    }
    
    func hamburgerViewTapped() {
        let navigationController = parent as! UINavigationController
        let containerViewController = navigationController.tabBarController!.parent as! ContainerViewController
        containerViewController.hideOrShowMenu(show: !containerViewController.showingMenu, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayText.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsViewController", for: indexPath)
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.text = self.arrayText[indexPath.row]
        cell.imageView?.image = self.arrayImage[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            showWaiting(completion: {
                if StoreManager.shared.productsFromStore.count > 0 {
                    let product = StoreManager.shared.productsFromStore[0]
                    StoreManager.shared.buy(product: product)
                }
            })
        }
        if indexPath.row == 1 {
            showWaiting(completion: {
                if StoreManager.shared.productsFromStore.count > 1 {
                    let product = StoreManager.shared.productsFromStore[1]
                    StoreManager.shared.buy(product: product)
                }
            })
        }
        
        if indexPath.row == 2 {
            showWaiting(completion: { 
                StoreManager.shared.restoreAllPurchases()
            })
        }
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    // MARK: Show waiting spinner
    func showWaiting(completion: (() -> Void)? = nil) -> Void {
        if alert == nil {
            alert = UIAlertController(title: "", message: "Please wait\n\n\n", preferredStyle: .alert)
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            spinner.center = CGPoint(x: 135.0, y: 65.5)
            spinner.color = #colorLiteral(red: 0.8689488173, green: 0.5551353097, blue: 0.5559484959, alpha: 1)
            spinner.startAnimating()
            alert?.view.addSubview(spinner)
        }
        let topVC = self.getCurrentViewController(self)!
        topVC.present(alert!, animated: true, completion: completion)
    }
    
    deinit {
        print("Deinit SettingsViewController ok")
    }

}
