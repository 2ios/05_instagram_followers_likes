//
//  ContainerViewController.swift
//  Menu
//
//  Created by Andrei Golban on 7/29/17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController, UIScrollViewDelegate {
    
    //MARK: Variables
    var detailViewController: DetailViewController?
    var followersViewController: FollowersViewController?
    var likesViewController: LikesViewController?
    var tagsViewController: TagsViewController?
    var settingsViewController: SettingsViewController?
    
    var showingMenu = false
    
    //MARK IBOutlets
    @IBOutlet weak var menuContainerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        menuContainerView.layer.anchorPoint = CGPoint(x: 1.0, y: 0.5)
        hideOrShowMenu(show: showingMenu, animated: false)
    }
    
    // MARK: ContainerViewController
    func hideOrShowMenu(show: Bool, animated: Bool) {
        let xOffset = menuContainerView.bounds.width
        scrollView.setContentOffset(show ? CGPoint(x: 0, y: 0):  CGPoint(x: xOffset, y: 0), animated: animated)
        showingMenu = show
    }
    
    // MARK: - UIScrollViewDelegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let multiplier = 1.0 / menuContainerView.bounds.width
        let offset = scrollView.contentOffset.x * multiplier
        let fraction = 1.0 - offset
        
        menuContainerView.layer.transform = transformForFraction(fraction: fraction)
        menuContainerView.alpha = fraction
        
        if let detailViewController = detailViewController {
            if let rotatingView = detailViewController.hamburgerView {
                rotatingView.rotate(fraction: fraction)
            }
        }
        if let followersViewController = followersViewController {
            if let rotatingView = followersViewController.hamburgerView {
                rotatingView.rotate(fraction: fraction)
            }
        }
        
        if let likesViewController = likesViewController {
            if let rotatingView = likesViewController.hamburgerView {
                rotatingView.rotate(fraction: fraction)
            }
        }
        if let tagsViewController = tagsViewController {
            if let rotatingView = tagsViewController.hamburgerView {
                rotatingView.rotate(fraction: fraction)
            }
        }
        if let settingsViewController = settingsViewController {
            if let rotatingView = settingsViewController.hamburgerView {
                rotatingView.rotate(fraction: fraction)
            }
        }
        /*
         Fix for the UIScrollView paging-related issue mentioned here:
         http://stackoverflow.com/questions/4480512/uiscrollview-single-tap-scrolls-it-to-top
         */
        scrollView.isPagingEnabled = scrollView.contentOffset.x < (scrollView.contentSize.width - scrollView.frame.width)
        
        let menuOffset = menuContainerView.bounds.width
        showingMenu = !CGPoint(x: menuOffset, y: 0).equalTo(scrollView.contentOffset)
    }
    
    // MARK: - Private
    func transformForFraction(fraction:CGFloat) -> CATransform3D {
        var identity = CATransform3DIdentity
        identity.m34 = -1.0 / 1000.0;
        let angle = Double(1.0 - fraction) * -.pi/2
        let xOffset = menuContainerView.bounds.width * 0.5
        let rotateTransform = CATransform3DRotate(identity, CGFloat(angle), 0.0, 1.0, 0.0)
        let translateTransform = CATransform3DMakeTranslation(xOffset, 0.0, 0.0)
        return CATransform3DConcat(rotateTransform, translateTransform)
    }
}

