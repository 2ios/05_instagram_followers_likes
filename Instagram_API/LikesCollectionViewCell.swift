//
//  LikesCollectionViewCell.swift
//  Instagram_API
//
//  Created by Andrei Golban on 03.08.17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import UIKit

class LikesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
}
