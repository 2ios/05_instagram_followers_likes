//
//  ViewController.swift
//  Instagram_API
//
//  Created by Andrei Golban on 20.07.17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: Variables
    var alert: UIAlertController?
    
    //MARK: IBOutlets
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var passWord: UITextField!
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBAction func loginButton(_ sender: UIButton) {
        view.endEditing(true)
        showWaiting {
            Swift_InstagramSDK.sharedInstance.checkNewHeaders()
            Swift_InstagramSDK.sharedInstance.login(email: self.userName.text!, password: self.passWord.text!) { (instagramUser, error) in
                if error == nil {
//                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
//                                                    object: nil,
//                                                    userInfo: ["title" : "nil", "message" : "ToContainerViewController"])
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "ContainerViewController")
                    
                    var topVC = self.getCurrentViewController(self)!
                    if topVC.isKind(of: UIAlertController.self) {
                        topVC.dismiss(animated: true, completion: {
                            topVC = self.getCurrentViewController(self)!
                            topVC.present(controller, animated: true, completion: nil)
                        })
                    }
                    
                    UserDefaults.standard.set(true, forKey: "LogIn")
                    let user = LoginForm(user: self.userName.text!, pass: self.passWord.text!)
                    user.saveLogin()
                } else {
                    self.userName.text! = ""
                    self.passWord.text! = ""
                    NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                                    object: nil,
                                                    userInfo: ["title" : "Sorry!", "message" : "\(error!.localizedDescription)"])
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.catchNotification(notification:)), name: NSNotification.Name(rawValue: "CatchNotification"), object: nil)
        
        userName.attributedPlaceholder = NSAttributedString(string: "username", attributes: [NSForegroundColorAttributeName: UIColor.white])
        passWord.attributedPlaceholder = NSAttributedString(string: "password", attributes: [NSForegroundColorAttributeName: UIColor.white])
        loginBtn.layer.borderWidth = 1
        loginBtn.layer.borderColor = UIColor.white.cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Keyboard control
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToContainerViewController" {
            NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                            object: nil,
                                            userInfo: ["title" : "nil", "message" : ""])
        }
    }
    
    // MARK: Show waiting spinner
    func showWaiting(completion: (() -> Void)? = nil) -> Void {
        if alert == nil {
            alert = UIAlertController(title: "", message: "Please wait\n\n\n", preferredStyle: .alert)
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            spinner.center = CGPoint(x: 135.0, y: 65.5)
            spinner.color = #colorLiteral(red: 0.8689488173, green: 0.5551353097, blue: 0.5559484959, alpha: 1)
            spinner.startAnimating()
            alert?.view.addSubview(spinner)
        }
        let topVC = self.getCurrentViewController(self)!
        topVC.present(alert!, animated: true, completion: completion)
    }
}
