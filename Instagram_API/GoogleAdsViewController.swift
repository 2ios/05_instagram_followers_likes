//
//  GoogleAdsViewController.swift
//  Instagram_API
//
//  Created by User543 on 09.08.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit
import GoogleMobileAds

class GoogleAdsViewController: UIViewController, GADInterstitialDelegate {
    
    var interstitial: GADInterstitial!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        
    }
    
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        var topVC = self.getCurrentViewController(self)!
        if topVC.isKind(of: UIAlertController.self) {
            topVC.dismiss(animated: true, completion: {
                topVC = self.getCurrentViewController(self)!
                self.interstitial.present(fromRootViewController: self)
            })
        } else {
            self.interstitial.present(fromRootViewController: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}
