//
//  DetailViewController.swift
//  Menu
//
//  Created by Andrei Golban on 7/29/17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import UIKit
import SDWebImage

class DetailViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    //MARK: Variables
    var alert: UIAlertController?
    var username: String = ""
    var mediaItems: [NSMutableDictionary] = []
    var mediaNextId: String?
    var boolStopGet = false
    
    var hamburgerView: HamburgerView?
    
    let typeImage = [#imageLiteral(resourceName: "ImageType"),#imageLiteral(resourceName: "AlbumType"),#imageLiteral(resourceName: "VideoType")]
    
    //MARK: IBOutlets
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var followersCount: UILabel!
    @IBOutlet weak var followingCount: UILabel!
    @IBOutlet weak var mediaCount: UILabel!
    @IBOutlet weak var infoView: UIView!
    
    @IBAction func logOut(_ sender: UIButton) {
        Swift_InstagramSDK.sharedInstance.logout(username: username) { (bool, error) in
            if bool {
                print("Logout succes")
                UserDefaults.standard.set(false, forKey: "LogIn")
//                self.dismiss(animated: true, completion: nil)
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
                self.present(controller, animated: true, completion: nil)
            } else {
                print(error?.localizedDescription as Any)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Remove the drop shadow from the navigation bar
        navigationController!.navigationBar.clipsToBounds = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hamburgerViewTapped))
        hamburgerView = HamburgerView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        hamburgerView!.addGestureRecognizer(tapGestureRecognizer)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: hamburgerView!)
        
        (navigationController!.tabBarController!.parent as! ContainerViewController).detailViewController = self
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        self.username = UserDefaults.standard.object(forKey: "curentUser") as! String
        
        navigationItem.title = self.username
        
        let infoViewImage = UIImageView(image: #imageLiteral(resourceName: "Menu"))
        infoViewImage.tag = 1000
        infoViewImage.autoresizingMask = [.flexibleLeftMargin,
            .flexibleWidth,
            .flexibleRightMargin,
            .flexibleTopMargin,
            .flexibleHeight,
            .flexibleBottomMargin]
        self.infoView.addSubview(infoViewImage)
        self.infoView.sendSubview(toBack: infoViewImage)
        let imageView = UIImageView(image: #imageLiteral(resourceName: "GradientBackground"))
        self.collectionView.backgroundView = imageView
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.mediaNextId = ""
        imageView.layer.cornerRadius = imageView.frame.height / 2
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if boolForAds {
            showWaiting {
                self.getInfoByName()
                self.getFeedMedia()
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let img = self.view.viewWithTag(1000)
        img?.frame.size = CGSize(width: self.view.frame.size.width, height: (img?.frame.size.height)!)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        SDImageCache.shared().clearMemory()
        SDImageCache.shared().clearDisk()
        self.mediaItems = []
        self.collectionView.reloadData()
    }
    
    func hamburgerViewTapped() {
        let navigationController = parent as! UINavigationController
        let containerViewController = navigationController.tabBarController!.parent as! ContainerViewController
        containerViewController.hideOrShowMenu(show: !containerViewController.showingMenu, animated: true)
    }
    
    // MARK: CollectionView Data Source
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.mediaItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailCollectionViewCell", for: indexPath) as! DetailCollectionViewCell
        if let photoUrl = mediaItems[indexPath.row].object(forKey: "photoUrl") as? String{
            let url = URL(string: photoUrl)
            cell.imageView.sd_setImage(with: url)
        }
        cell.likeCount.text = String(describing: mediaItems[indexPath.row].object(forKey: "likeCount")!)
        let type = mediaItems[indexPath.row].object(forKey: "Type") as? String
        
        if type == "Image" {
            cell.typeImage.image = typeImage[0]
        }
        if type == "Carousel" {
            cell.typeImage.image = typeImage[1]
        }
        
        if type == "Video" {
            cell.typeImage.image = typeImage[2]
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var n = 0
        if UI_USER_INTERFACE_IDIOM() == .pad {
            n = 6
        } else {
            n = 3
        }
        let width = self.collectionView.frame.width - CGFloat((n-1)*10)
        let widthCell = width / CGFloat(n)
        let heightCell = widthCell
        return CGSize(width: widthCell, height: heightCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let lastRowIndex = collectionView.numberOfItems(inSection: 0) - 1
        if indexPath.item == lastRowIndex && !boolStopGet && self.mediaNextId != nil {
            DispatchQueue.main.async{
                self.getFeedMedia()
            }
        }
    }
    // MARK: Functions for requests.
    func getInfoByName() {
        Swift_InstagramSDK.sharedInstance.getInfoByName(username: username) { (dict, error) in
            if error == nil {
                let user = dict!.object(forKey: "user") as! NSDictionary
                let photoUrl = user.object(forKey: "profile_pic_url") as! String
                let fullName = user.object(forKey: "full_name") as! String
                let followersCount = user.object(forKey: "follower_count") as! Int64
                let followingCount = user.object(forKey: "following_count") as! Int64
                let postCount = user.object(forKey: "media_count") as! Int64
                self.imageView.sd_setImage(with: URL(string: photoUrl))
                self.fullName.text = fullName
                self.followersCount.text = String(describing: followersCount)
                self.followingCount.text = String(describing: followingCount)
                self.mediaCount.text = String(describing: postCount)
            } else {
                print(error!.localizedDescription)
            }
            DispatchQueue.main.async {
                print("GET Info is completed")
            }
        }
    }
    
    func getFeedMedia() {
        Swift_InstagramSDK.sharedInstance.getFeedMedia(username: username, max_next_id: mediaNextId!) { (dict, error) in
            if error == nil {
                let feedMedia = dict!.object(forKey: "items") as! NSArray
                for i in 0..<feedMedia.count {
                    let dictionary = NSMutableDictionary()
                    let media = feedMedia[i] as! NSDictionary
                    if let carouselMedia = media.object(forKey: "carousel_media") as? NSArray {
                        let media = carouselMedia[0] as! NSDictionary
                        if let videoVersions = media.object(forKey: "video_versions") as? NSDictionary {
                            if let photoVersion = videoVersions.object(forKey: "image_versions2") as? NSDictionary {
                                let photoCanditates = photoVersion.object(forKey: "candidates") as! NSArray
                                let photo = photoCanditates[1] as! NSDictionary
                                let photoUrl = photo.object(forKey: "url") as! String
                                dictionary.setObject(photoUrl, forKey: "photoUrl" as NSCopying)
                                dictionary.setObject("Carousel", forKey: "Type" as NSCopying)
                            }
                        }
                        else{
                            let imgageVersions = media.object(forKey: "image_versions2") as! NSDictionary
                            let photoCanditates = imgageVersions.object(forKey: "candidates") as! NSArray
                            let photo = photoCanditates[1] as! NSDictionary
                            let photoUrl = photo.object(forKey: "url") as! String
                            dictionary.setObject(photoUrl, forKey: "photoUrl" as NSCopying)
                            dictionary.setObject("Carousel", forKey: "Type" as NSCopying)
                        }
                    } else if media.object(forKey: "video_versions") != nil{
                        if let photoVersion = media.object(forKey: "image_versions2") as? NSDictionary {
                            let photoCanditates = photoVersion.object(forKey: "candidates") as! NSArray
                            let photo = photoCanditates[1] as! NSDictionary
                            let photoUrl = photo.object(forKey: "url") as! String
                            dictionary.setObject(photoUrl, forKey: "photoUrl" as NSCopying)
                            dictionary.setObject("Video", forKey: "Type" as NSCopying)
                        }
                    } else {
                        if let photoVersion = media.object(forKey: "image_versions2") as? NSDictionary{
                            let photoCanditates = photoVersion.object(forKey: "candidates") as! NSArray
                            let photo = photoCanditates[1] as! NSDictionary
                            let photoUrl = photo.object(forKey: "url") as! String
                            dictionary.setObject(photoUrl, forKey: "photoUrl" as NSCopying)
                            dictionary.setObject("Image", forKey: "Type" as NSCopying)
                        }
                    }
                    let likeCount = media.object(forKey: "like_count") as! Int64
                    dictionary.setObject(likeCount, forKey: "likeCount" as NSCopying)
                    self.mediaItems.append(dictionary)
                    if dict!.object(forKey: "next_max_id") != nil {
                        self.mediaNextId = dict!.object(forKey: "next_max_id") as? String
                        self.boolStopGet = false
                    } else {
                        self.mediaNextId = nil
                        self.boolStopGet = true
                    }
                }
            } else {
                print(error!.localizedDescription)
            }
            DispatchQueue.main.async {
                NotificationCenter.default.post(name:NSNotification.Name(rawValue: "CatchNotification"),
                                                object: nil,
                                                userInfo: ["title" : "nil", "message" : "Hide"])
                self.collectionView.reloadData()
                print("GET FeedMedia is completed")
            }
        }
    }
    
    // MARK: Show waiting spinner
    func showWaiting(completion: (() -> Void)? = nil) -> Void {
        if alert == nil {
            alert = UIAlertController(title: "", message: "Please wait\n\n\n", preferredStyle: .alert)
            let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
            spinner.center = CGPoint(x: 135.0, y: 65.5)
            spinner.color = #colorLiteral(red: 0.8689488173, green: 0.5551353097, blue: 0.5559484959, alpha: 1)
            spinner.startAnimating()
            alert?.view.addSubview(spinner)
        }
        let topVC = self.getCurrentViewController(self)!
        topVC.present(alert!, animated: true, completion: completion)
    }
    
    deinit {
        print("Deinit DetailViewController ok")
    }
}

