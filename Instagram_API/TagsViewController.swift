//
//  StoreCoinsViewController.swift
//  Menu
//
//  Created by Andrei Golban on 7/30/17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import UIKit

class TagsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    //MARK: Variables
    
    var hamburgerView: HamburgerView?
    var index = 0
    
    //MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Remove the drop shadow from the navigation bar
        navigationController!.navigationBar.clipsToBounds = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.hamburgerViewTapped))
        hamburgerView = HamburgerView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        hamburgerView!.addGestureRecognizer(tapGestureRecognizer)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: hamburgerView!)
        
        (navigationController!.tabBarController!.parent as! ContainerViewController).tagsViewController = self
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        navigationItem.title = "Tags"
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.backgroundView = UIImageView(image: #imageLiteral(resourceName: "GradientBackground"))
    }
    
    func hamburgerViewTapped() {
        let navigationController = parent as! UINavigationController
        let containerViewController = navigationController.tabBarController!.parent as! ContainerViewController
        containerViewController.hideOrShowMenu(show: !containerViewController.showingMenu, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TagsViewCell", for: indexPath)
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.text = categories[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        index = indexPath.row
        performSegue(withIdentifier: "ToSubTagsViewController", sender: nil)
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToSubTagsViewController" {
            let subTagsVC: SubTagsViewController = segue.destination as! SubTagsViewController
            subTagsVC.subCategories = categoriesTags[index]!
            subTagsVC.dictionary = myDictionary[index]!
        }
    }
    
    deinit {
        print("Deinit StoreCoinsTableViewController ok")
    }
}
