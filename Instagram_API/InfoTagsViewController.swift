//
//  InfoTagsViewController.swift
//  Instagram_API
//
//  Created by User543 on 08.08.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class InfoTagsViewController: UIViewController {

    @IBOutlet weak var textField: UITextView!
    
    @IBAction func buttonCopy(_ sender: UIButton) {
         UIPasteboard.general.string = textField.text
    }
    
    var text: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        textField.text = text
        navigationItem.title = "Hashtags"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
