//
//  LoginForm.swift
//  Instagram_API
//
//  Created by Andrei Golban on 21.07.17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import Foundation

class LoginForm {
    
    private var username: String!
    private var password: String!
    
    init(user: String, pass: String) {
        self.username = user
        self.password = pass
    }
    
    func saveLogin() {
        UserDefaults.standard.set(self.username, forKey: "Username")
        UserDefaults.standard.set(self.password, forKey: "Password")
    }
}
