//
//  DetailCollectionViewCell.swift
//  Instagram_API
//
//  Created by Andrei Golban on 01.08.17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import UIKit

class DetailCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var likeCount: UILabel!
    @IBOutlet weak var typeImage: UIImageView!
}
