//
//  Tags.swift
//  InstaAnalytics
//
//  Created by User78 on 7/27/17.
//  Copyright © 2017 User78. All rights reserved.
//

import Foundation

var boolForAds = true

let categories = [ "Popular",
                   "Nature",
                   "Weather",
                   "Animals",
                   "Social",
                   "Holidays",
                   "Family",
                   "Art",
                   "Urban",
                   "Food",
                   "Fashion",
                   "Celebrities",
                   "Entertaiment",
                   "Electronics",
                   "Follow",
                   "Travel & Sports",
                   "Other"
]

let myDictionary = [ 0 : popularTags,
                   1 : natureTags,
                   2 : weatherTags,
                   3 : animalsTags,
                   4 : socialTags,
                   5 : holidayTags,
                   6 : familyTags,
                   7 : artTags,
                   8 : urbanTags,
                   9 : foodTags,
                   10 : fashionTags,
                   11 : celebritiesTags,
                   12 : entertaimentTags,
                   13 : electronicTags,
                   14 : followTags,
                   15 : travelTags,
                   16 : otherTags
]

let categoriesTags = [ 0 : ["Most Popular", "2ndPopular", "3rd Popular"],
                       1 : ["Nature General", "Beach", "Sunset & Sunrise", "Flowers"],
                       2 : ["Sunny", "Cloudy", "Raining", "Snowing", "Spring", "Summer", "Fall", "Winter"],
                       3 : ["Animals General", "Dogs", "Cats", "Horses", "Insects", "Fish"],
                       4 : ["General", "Selfies", "Girls", "Guys", "Love", "Friends", "Good Morning", "Good Night"],
                       5 : ["Party", "Birthday", "Wedding", "Thanksgiving", "Halloween", "Christmas", "New Year", "Holidays"],
                       6 : ["Family General", "Babies", "Kids"],
                       7 : ["Art", "Photography", "HDR", "Black & White", "Minimalism", "Abstract", "Instagram"],
                       8 : ["Architecture", "Street Art", "VSCO", "VSCO 2"],
                       9 : ["Food General", "Dessert", "Drinks", "Coffee", "Tea"],
                       10 : ["Fashion General", "Fashion (Girls)", "Fashion (Guys)", "OOTD / Outfit of the Day", "Nails", "Hair", "Makeup", "Jewelry", "Bracelets", "Earrings", "High Heels", "Sneakers", "Tattoos", "Piercings"],
                       11 : ["One Direction", "Justin Bieber", "Taylor Swift", "Katy Perry", "Liam Payne", "Nial Horan", "Louis Tomlinson", "Zayn Malik", "Harry Styles", "Lil Wayne", "Drake", "Selena Gomez", "Miley Cyrus", "Demi Lovato", "Ariana Grande", "Chris Brown", "Rihanna", "Austin Mahone", "5 Seconds of Summer"],
                       12 : ["Music", "Movies", "Books", "Video Games"],
                       13 : ["Electronics General", "iPhone", "Android"],
                       14 : ["FSLC ", "Follow", "Shoutout", "Like", "Comment"],
                       15 : ["Travel", "Cars", "Motorcycles", "Skateboarding", "Health & Fitness", "Bodybuilding", "Sports", "Running", "Dance", "Cheerleading", "Gymnastics", "Basketball", "Soccer", "Football", "Baseball", "Hockey"],
                       16 : ["School", "Work", "Funny", "Quotes", "Throwback Thursday", "Instagram Direct", "Tumblr", "Kik", "Snapchat", "Ask.fm", "Money", "Colors", "Spiritual"]
]

let popularTags = ["#love #instagood #photooftheday #Beautiful #fashion #like4like #picoftheday #happy #followme #art #follow #instadaily #style #instalike #cute #life #Selfie #instagram #likeforlike #fun #me #TagsForLikes #girl #nature #amazing #travel #beauty #fitness #smile #follow4follow",
                   "#fun #instagramers #TagsForLikes #TagsForLikesApp #food #smile #pretty #followme #nature #lol #dog #hair #onedirection #sunset #swag #throwbackthursday #instagood #beach #statigram #friends #hot #funny #blue #life #art #instahub #photo #cool #pink #bestoftheday #clouds",
                   "#amazing #TagsForLikes #TagsForLikesApp #followme #all_shots #textgram #family #instago #igaddict #awesome #girls #instagood #my #bored #baby #music #red #green #water #harrystyles #bestoftheday #black #party #white #yum #flower #2012 #night #instalove #niallhoran #jj_forum"
]

let natureTags = ["#nature #TagsForLikes #TagsForLikesApp #sky #sun #summer #beach #beautiful #pretty #sunset #sunrise #blue #flowers #night #tree #twilight #clouds #beauty #light #cloudporn #photooftheday #love #green #skylovers #dusk #weather #day #red #iphonesia #mothernature",
                  "#beach #sun #nature #water #TagsForLikes #TagsForLikesApp #TFLers #ocean #lake #instagood #photooftheday #beautiful #sky #clouds #cloudporn #fun #pretty #sand #reflection #amazing #beauty #beautiful #shore #waterfoam #seashore #waves #wave",
                  "#sunset #sunrise #sun #TagsForLikes #TagsForLikesApp #TFLers #pretty #beautiful #red #orange #pink #sky #skyporn #cloudporn #nature #clouds #horizon #photooftheday #instagood #gorgeous #warm #view #night #morning #silhouette #instasky #all_sunsets",
                  "#flowers #flower #TagsForLikes #petal #petals #nature #beautiful #love #pretty #plants #blossom #sopretty #spring #summer #flowerstagram #flowersofinstagram #flowerstyles_gf #flowerslovers #flowerporn #botanical #floral #florals #insta_pick_blossom #flowermagic #instablooms #bloom #blooms #botanical #floweroftheday"
]

let weatherTags = ["#sun #sunny #sunnyday #TagsForLikes #TagsForLikesApp #sunnydays #sunlight #light #sunshine #shine #nature #sky #skywatcher #thesun #sunrays #photooftheday #beautiful #beautifulday #weather #summer #goodday #goodweather #instasunny #instasun #instagood #clearskies #clearsky #blueskies #lookup #bright #brightsun",
                   "#clouds #cloud #cloudporn #TagsForLikes #TagsForLikesApp #weather #lookup #sky #skies #skyporn #cloudy #instacloud #instaclouds #instagood #nature #beautiful #gloomy #skyline #horizon #overcast #instasky #epicsky #crazyclouds #photooftheday #cloud_skye #skyback #insta_sky_lovers #iskyhub",
                   "#rain #raining #rainyday #TagsForLikes #TagsForLikesApp #pouring #rainydays #water #clouds #cloudy #photooftheday #puddle #umbrella #instagood #gloomy #rainyweather #rainydayz #splash #TFLers #downpour #instarain",
                   "#snow #snowing #winter #TagsForLikes #cold #ice #white #weather #sky #skies #frosty #frost #chilly #TFLers #nature #snowflakes #instagood #instawinter #instasnow #photooftheday #snowfall #blizzard",
                   "#spring #blossom #flowers #TagsForLikes #beautiful #season #seasons #instaspring #instagood #springtime #color #ilovespring #warm #sunny #sun #tree #pretty #TFLers #trees #flower #bloom #colorful",
                   "#summer #summertime #sun #TagsForLikes #hot #sunny #warm #fun #beautiful #sky #clearskys #season #seasons #instagood #instasummer #photooftheday #nature #TFLers #clearsky #bluesky #vacationtime #weather #summerweather #sunshine #summertimeshine",
                   "#fall #autumn #leaves #TagsForLikes #falltime #season #seasons #instafall #instagood #TFLers #instaautumn #photooftheday #leaf #foliage #colorful #orange #red #autumnweather #fallweather #nature",
                   "#winter #cold #holidays #TagsForLikes #TagsForLikesApp #snow #rain #christmas #snowing #blizzard #snowflakes #wintertime #staywarm #cloudy #instawinter #instagood #holidayseason #photooftheday #season #seasons #nature"
]

let animalsTags = [ "#animals #animal #pet #TagsForLikes #TagsForLikesApp #dog #cat #dogs #cats #photooftheday #cute #pets #instagood #animales #cute #love #nature #animallovers #pets_of_instagram #petstagram #petsagram",
                    "#dog #dog #puppy #pup #TagsForLikes #TagsForLikesApp #cute #eyes #instagood #dogs_of_instagram #pet #pets #animal #animals #petstagram #petsagram #dogsitting #photooftheday #dogsofinstagram #ilovemydog #instagramdogs #nature #dogstagram #dogoftheday #lovedogs #lovepuppies #hound #adorable #doglover #instapuppy #instadog",
                    "#cat #cats #TagsForLikes #TagsForLikesApp #catsagram #catstagram #instagood #kitten #kitty #kittens #pet #pets #animal #animals #petstagram #petsagram #photooftheday #catsofinstagram #ilovemycat #instagramcats #nature #catoftheday #lovecats #furry #sleeping #lovekittens #adorable #catlover #instacat",
                    "#horses #horse #horsesofinstagram #TagsForLikes #TagsForLikesApp #horseshow #horseshoe #horses_of_instagram #horsestagram #instahorses #wild #mane #instagood #grass #field #farm #nature #pony #ponies #ilovemyhorse #babyhorse #beautiful #pretty #photooftheday #gallop #jockey #rider #riders #riding",
                    "#insects #insect #bug #bugs #TagsForLikes #TagsForLikesApp #bugslife #macro #closeup #nature #animals #animals #instanature #instagood #macrogardener #macrophotography #creature #creatures #macro_creature_feature #photooftheday #wildlife #nature_shooters #earth #naturelover #lovenature",
                    "#fish #aquarium #fishtank #TagsForLikes #TagsForLikesApp #fishporn #instafish #instagood #swim #swimming #water #coral #reef #reeftank #tropical #tropicalfish #aquaria #photooftheday #saltwater #freshwater #beautiful #ocean #watertank"
]

let socialTags = [ "#love #TagsForLikes #TagsForLikesApp #photooftheday #me #instamood #cute #igers #picoftheday #girl #guy #beautiful #fashion #instagramers #follow #smile #pretty #followme #friends #hair #swag #photo #life #funny #cool #hot #bored #portrait #baby #girls #iphonesia",
                   "#selfie #selfienation #selfies #TagsForLikes #TFLers #TagsForLikesApp #me #love #pretty #handsome #instagood #instaselfie #selfietime #face #shamelessselefie #life #hair #portrait #igers #fun #followme #instalove #smile #igdaily #eyes #follow",
                   "#girl #girls #love #TagsForLikes #TFLers #me #cute #picoftheday #beautiful #photooftheday #instagood #fun #smile #pretty #follow #followme #hair #friends #lady #swag #hot #cool #kik #fashion #igers #instagramers #style #sweet #eyes #beauty",
                   "#guys #guy #boy #TagsForLikes #TFLers #boys #love #me #cute #handsome #picoftheday #photooftheday #instagood #fun #smile #dude #follow #followme #swag #hot #cool #kik #igers #instagramers #eyes",
                   "#love #couple #cute #adorable #TagsForLikes #TagsForLikesApp #kiss #kisses #hugs #romance #forever #girlfriend #boyfriend #gf #bf #bff #together #photooftheday #happy #me #girl #boy #beautiful #instagood #instalove #loveher #lovehim #pretty #fun #smile #xoxo",
                   "#friend #friends #fun #TagsForLikes #TagsForLikesApp #funny #love #instagood #igers #friendship #party #chill #happy #cute #photooftheday #live #forever #smile #bff #bf #gf #best #bestfriend #lovethem #bestfriends #goodfriends #besties #awesome #memories #goodtimes #goodtime",
                   "#goodmorning #morning #day #TagsForLikes #daytime #sunrise #morn #awake #wakeup #wake #wakingup #ready #sleepy #breakfast #tired #sluggish #bed #snooze #instagood #earlybird #sky #photooftheday #gettingready #goingout #sunshine #instamorning #work #early #fresh #refreshed",
                   "#goodnight #night #nighttime #TagsForLikes #sleep #sleeptime #sleepy #sleepyhead #tired #goodday #instagood #instagoodnight #photooftheday #nightynight #lightsout #bed #bedtime #rest #nightowl #dark #moonlight #moon #out #passout #knockout #knockedout"
]
let holidayTags = [ "#party #partying #fun #TagsForLikes #TagsForLikesApp #instaparty #instafun #instagood #bestoftheday #crazy #friend #friends #besties #guys #girls #chill #chilling #kickit #kickinit #cool #love #memories #night #smile #music #outfit #funtime #funtimes #goodtime #goodtimes #happy",
                    "#birthday #bday #party #TagsForLikes #instabday #bestoftheday #birthdaycake #cake #friends #celebrate #photooftheday #instagood #candle #candles #happy #young #old #years #instacake #happybirthday #instabirthday #born #family",
                    "#wedding #party #weddingparty #TagsForLikes #celebration #bride #groom #bridesmaids #happy #happiness #unforgettable #love #forever #weddingdress #weddinggown #weddingcake #family #smiles #together #ceremony #romance #marriage #weddingday #flowers #celebrate #instawed #instawedding #party #congrats #congratulations",
                    "#thanksgiving #thanks #giving #TagsForLikes #turkey #turkeyday #food #foodporn #holiday #family #friends #love #instagood #photooftheday #happythanksgiving #celebrate #stuffing #feast #thankful #blessed #fun",
                    "#halloween #oct #october #31 #TagsForLikes #scary #spooky #boo #scared #costume #ghost #pumpkin #pumpkins #pumpkinpatch #carving #candy #orange #jackolantern #creepy #fall #trickortreat #trick #treat #instagood #party #holiday #celebrate #bestoftheday #hauntedhouse #haunted",
                    "#christmas #holidays #tistheseason #TagsForLikesApp #TagsForLikes #holiday #winter #instagood #happyholidays #elves #lights #presents #gifts #gift #tree #decorations #ornaments #carols #santa #santaclaus #christmas2014 #photooftheday #love #xmas #red #green #christmastree #family #jolly #snow #merrychristmas",
                    "#happynewyear #newyearsday #newyear #TagsForLikesApp #TagsForLikes #2015 #2014 #newyearseve #newyears #newyears2015 #bye2014 #hello2015 #donewith2014 #TFLers #newyearsresolution #goals #dec31 #jan1 #dec312013 #jan12014 #instagood #celebration #photooftheday #newyearscelebration #newyearsparty #party #celebrate #fun",
                    "#happyholidays #holidays #holiday #TagsForLikesApp #TagsForLikes #vacation #winter2014 #2014 #2015 #happyholidays2014 #presents #parties #fun #happy #family #love"
]
let familyTags = [ "#family #fam #mom #dad #TagsForLikes #brother #sister #brothers #sisters #bro #sis #siblings #love #instagood #father #mother #related #fun #photooftheday #children #kids #life #happy #familytime #cute #smile #fun",
                   "#baby #babies #adorable #cute #TagsForLikes #cuddly #cuddle #small #lovely #love #instagood #kid #kids #beautiful #life #sleep #sleeping #children #happy #igbabies #childrenphoto #toddler #instababy #infant #young #photooftheday #sweet #tiny #little #family",
                   "#kids #kid #instakids #TagsForLikes #child #children #childrenphoto #love #cute #adorable #instagood #young #sweet #pretty #handsome #little #photooftheday #fun #family #baby #instababy #play #happy #smile #instacute"
]
let artTags = [ "#art #illustration #drawing #draw #TagsForLikes #picture #artist #sketch #sketchbook #paper #pen #pencil #artsy #instaart #beautiful #instagood #gallery #masterpiece #creative #photooftheday #instaartist #graphic #graphics #artoftheday",
                "#photo #photos #pic #pics #TagsForLikes #picture #pictures #snapshot #art #beautiful #instagood #picoftheday #photooftheday #color #all_shots #exposure #composition #focus #capture #moment",
                "#hdr #hdriphoneographer #TagsForLikes #hdrspotters #hdrstyles_gf #hdri #hdroftheday #hdriphonegraphy #hdrepublic #hdr_lovers #awesome_hdr #instagood #hdrphotography #photooftheday #hdrimage #hdr_gallery #hdr_love #hdrfreak #hdrama #hdrart #hdrphoto #hdrfusion #hdrmania #hdrstyles #ihdr #str8hdr #hdr_edits",
                "#blackandwhite #bnw #monochrome #TagsForLikes #instablackandwhite #monoart #insta_bw #bnw_society #bw_lover #bw_photooftheday #photooftheday #bw #instagood #bw_society #bw_crew #bwwednesday #insta_pick_bw #bwstyles_gf #irox_bw #igersbnw #bwstyleoftheday #monotone #monochromatic#noir #fineart_photobw",
                "#minimalism #minimalist #minimal #TagsForLikes #minimalistic #minimalistics #minimalove #minimalobsession #photooftheday #minimalninja #instaminim #minimalisbd #simple #simplicity #keepitsimple #minimalplanet #love #instagood #minimalhunter #minimalista #minimalismo #beautiful #art #lessismore #simpleandpure #negativespace",
                "#abstract #art #abstractart #TagsForLikes #abstracters_anonymous #abstract_buff #abstraction #instagood #creative #artsy #beautiful #photooftheday #abstracto #stayabstract #instaabstract",
                "#instagrammers #igers #TagsForLikes #instalove #instamood #instagood #followme #follow #comment #shoutout #iphoneography #androidography #filter #filters #hipster #contests #photo #instadaily #igaddict #TFLers #photooftheday #pics #insta #picoftheday #bestoftheday #instadaily #instafamous #popularpic #popularphoto"
]
let urbanTags = [ "#architecture #building #TagsForLikes #architexture #city #buildings #skyscraper #urban #design #minimal #cities #town #street #art #arts #architecturelovers #abstract #lines #instagood #beautiful #archilovers #architectureporn #lookingup #style #archidaily #composition #geometry #perspective #geometric #pattern",
                  "#streetart #street #streetphotography #TagsForLikes #sprayart #urban #urbanart #urbanwalls #wall #wallporn #graffitiigers #stencilart #art #graffiti #instagraffiti #instagood #artwork #mural #graffitiporn #photooftheday #stencil #streetartistry #stickerart #pasteup #instagraff #instagrafite #streetarteverywhere",
                  "#vsco #vscocam #vscogood #TagsForLikes #vscophile #vscogrid #vscogram #vscorussia #vscodaily #liveauthentic #vscobest #bestofvsco #livefolk #vscoedit #vscofilm #vsco_hub #vscofeature #vscoonly #TagsForLikesApp #justgoshoot #vsconature #vscolove #vscophoto #vscobrasil #vscostyle #vscoturkey #vscoaward #topvsco #instavsco #vscolover #vscomoment",
                  "#vscoeurope #afterlight #vscolife #TagsForLikes #vscoism #vscovisuals #vscoapp #vscoartist #vscogallery #vscoph #vscocamphotos #visualsoflife #igmasters #visualsgang #vscolovers #vscovibe #letsgosomewhere #TagsForLikesApp #neverstopexploring #vscoexpo #vscocamgram #vscogang #streetdreamsmag #vscocamonly #socality #vscomania #lifeofadventure #vscocool #vscomoscow #peoplescreatives #thatsdarling"
]
let foodTags = [ "#food #foodporn #yum #instafood #TagsForLikes #yummy #amazing #instagood #photooftheday #sweet #dinner #lunch #breakfast #fresh #tasty #food #delish #delicious #eating #foodpic #foodpics #eat #hungry #foodgasm #hot #foods",
                 "#dessert #food #desserts #TagsForLikes #yum #yummy #amazing #instagood #instafood #sweet #chocolate #cake #icecream #dessertporn #delish #foods #delicious #tasty #eat #eating #hungry #foodpics #sweettooth",
                 "#drink #drinks #slurp #TagsForLikes #pub #bar #liquor #yum #yummy #thirst #thirsty #instagood #cocktail #cocktails #drinkup #glass #can #photooftheday #beer #beers #wine",
                 "#coffee #cafe #instacoffee #TagsForLikes #cafelife #caffeine #hot #mug #drink #coffeeaddict #coffeegram #coffeeoftheday #cotd #coffeelover #coffeelovers #coffeeholic #coffiecup #coffeelove #coffeemug #TagsForLikesApp #coffeeholic #coffeelife",
                 "#tea #teatime #instatea #TagsForLikes #tealife #ilovetea #teaaddict #tealover #tealovers #teagram #healthy #drink #hot #mug #teaoftheday #teacup #teastagram #TagsForLikesApp #teaholic #tealove #tealife"
]
let fashionTags = [ "#fashion #style #stylish #love #TagsForLikes #me #cute #photooftheday #nails #hair #beauty #beautiful #instagood #pretty #swag #pink #girl #girls #eyes #design #model #dress #shoes #heels #styles #outfit #purse #jewelry #shopping #glam",
                    "#fashion #style #stylish #love #TagsForLikes #me #cute #photooftheday #nails #hair #beauty #beautiful #instagood #instafashion #pretty #girly #pink #girl #girls #eyes #model #dress #skirt #shoes #heels #styles #outfit #purse #jewelry #shopping",
                    "#fashion #swag #style #stylish #TagsForLikes #me #swagger #cute #photooftheday #jacket #hair #pants #shirt #instagood #handsome #cool #polo #swagg #guy #boy #boys #man #model #tshirt #shoes #sneakers #styles #jeans #fresh #dope",
                    "#ootd #outfitoftheday #lookoftheday #TagsForLikes #TFLers #fashion #fashiongram #style #love #beautiful #currentlywearing #lookbook #wiwt #whatiwore #whatiworetoday #ootdshare #outfit #clothes #wiw #mylook #fashionista #todayimwearing #instastyle #TagsForLikesApp #instafashion #outfitpost #fashionpost #todaysoutfit #fashiondiaries",
                    "#nails #nail #fashion #style #TagsForLikes #cute #beauty #beautiful #instagood #pretty #girl #girls #stylish #sparkles #styles #gliter #nailart #art #opi #photooftheday #essie #unhas #preto #branco #rosa #love #shiny #polish #nailpolish #nailswag",
                    "#hair #hairstyle #instahair #TagsForLikes #hairstyles #haircolour #haircolor #hairdye #hairdo #haircut #longhairdontcare #braid #fashion #instafashion #straighthair #longhair #style #straight #curly #black #brown #blonde #brunette #hairoftheday #hairideas #braidideas #perfectcurls #hairfashion #hairofinstagram #coolhair",
                    "#makeup #instamakeup #cosmetic #cosmetics #TagsForLikes #TFLers #fashion #eyeshadow #lipstick #gloss #mascara #palettes #eyeliner #lip #lips #tar #concealer #foundation #powder #eyes #eyebrows #lashes #lash #glue #glitter #crease #primers #base #beauty #beautiful",
                    "#jewelry #jewels #jewel #TagsForLikes #fashion #gems #gem #gemstone #bling #stones #stone #trendy #accessories #love #crystals #beautiful #ootd #style #fashionista #accessory #instajewelry #stylish #cute #jewelrygram #TagsForLikesApp #fashionjewelry",
                    "#bracelets #bracelet #TagsForLikes #armcandy #armswag #wristgame #pretty #love #beautiful #braceletstacks #trendy #instagood #fashion #braceletsoftheday #jewelry #fashionlovers #fashionista #accessories #TagsForLikesApp #armparty #wristwear",
                    "#earrings #earring #earringsoftheday #TagsForLikes #jewelry #fashion #accessories #earringaddict #earringstagram #fashionista #girl #stylish #love #beautiful #piercing #piercings #pierced #cute #gorgeous #trendy #earringswag #TagsForLikesApp #earringfashion #earringlove",
                    "#highheels #heels #platgorm #TagsForLikes #fashion #style #stylish #love #cute #photooftheday #tall #beauty #beautiful #instafashion #girl #girls #model #shoes #styles #outfit #instaheels #fashionshoes #shoelover #instashoes #highheelshoes #trendy #heelsaddict #loveheels #iloveheels #shoestagram",
                    "#shoes #shoe #kicks #TagsForLikes #instashoes #instakicks #sneakers #sneaker #sneakerhead #sneakerheads #solecollector #soleonfire #nicekicks #igsneakercommunity #sneakerfreak #sneakerporn #shoeporn #fashion #swag #instagood #fresh #photooftheday #nike #sneakerholics #sneakerfiend #shoegasm #kickstagram #walklikeus #peepmysneaks #flykicks",
                    "#tattoo #tattoos #tat #ink #inked #TagsForLikes #TFLers #tattooed #tattoist #coverup #art #design #instaart #instagood #sleevetattoo #handtattoo #chesttattoo #photooftheday #tatted #instatattoo #bodyart #tatts #tats #amazingink #tattedup #inkedup",
                    "#piercing #piercings #pierced #TagsForLikes #TFLers #bellyrings #navel #earlobe #ear #photooftheday #bellybuttonring #lipring #instagood #modifications #bodymods #piercingaddict #bellybar #bellybuttonpiercing"
]
let celebritiesTags = [ "#onedirection #TagsForLikesApp #harrystyles #niallhoran #zaynmalik #louistomlinson #liampayne #TagsForLikes #1d #directioner #1direction #niall #harry #zayn #liam #louis #leeyum #zjmalik #iphonesia #hot #love #cute #happy #beautiful #boys #guys #instagood #photooftheday",
                        "#justin #bieber #bieberfever #TagsForLikes #TFLers #beiber #beiberfever #justinbieberswag #hot #cool #boyfriend #justindrewbieber #instagood #photooftheday #juju #justinb #justindb #justinb #forever #biebs #neversaynever #belieber #believe #believetour #TagsForLikesApp #bieberlove",
                        "#taylor #swift #taylorswift #music #TagsForLikes #TFLers #country #singer #singing #song #instagood #swifties #sweet #cute #beautiful #love #girl #pretty #swiftie #flawless #photooftheday #awesome #tswift #TagsForLikesApp #lovesong",
                        "#katy #perry #katyperry #TagsForLikes #TFLers #pretty #beautiful #music #lovethissong #kp #katykats #katykat #katycats #katycat #caligirls #californiagirls #partofme #smile #instagood #instaperry #love #photooftheday #extraterrestrial #teenagedream #wideawake",
                        "#liampayne #liam #payne #onedirection #TagsForLikes #1d #directioner #1direction #photooftheday#leeyum #leeyumm #leeyumpayne #brilliam #hot #love #instagood #cute #happy #boys #guys",
                        "#niallhoran #niall #horan #onedirection #TagsForLikes #1d #directioner #1direction #hot #love #cute #happy #boys #guys #nialler #niallerhoran #photooftheday #niallerwins #niallers #guy #instagood #nialljameshoran #niallisbeautiful #nialhoranfacts #niallimagine #niallisperfect",
                        "#louistomlinson #louis #tomlinson #onedirection #TagsForLikes #1d #directioner #1direction #hot #love #cute #happy #boys #guys #louistomlinsonfacts #louistomlinsontohostteenchoice2013 #louistomlinsonimagine #louistomlinsonfanfic #louistomlinsonfact #tommo #lilo #loulou #lou #boobear #fabulouis",
                        "#zaynmalik #zayn #malik #TagsForLikes #onedirection #1d #directioner #malikbaby #hot #love #cute #happy #boys #guys #handsome #cool #instagood #directioners #zainmalik #TFLers #loveonedirection #photooftheday #lovezayn #lovemalik #lovedirectioners #welovezayn #amazayn #djmalik",
                        "#harrystyles #harry #styles #TagsForLikes #onedirection #1d #directioner #1direction #hot #love #cute #happy #boys #guys #photooftheday #harrystylesimagine #instagood #hazza #hazzastyles #harryedwardstyles #lovedirectioners #harreh #harold #haroldstyles #harryimagine",
                        "#lilwayne @lilwayneofficial_ #lil #wayne #liltunechi #weezy #ymcmb #youngmoney #cashmoney #tunechi #wayniac #weezyf #teamweezy #teamtunechi #trukfit #skate #famous #follow #rapper #tattoo #rich #singer #artist #hiphop #music #thecarter #artist",
                        "#drake #drizzy #drizzydrake #TagsForLikes #drakequotes #ymcmb #ovoxo #ovo #xo #teamdrizzy #teamdrake #instadrake #instagood #yolo #takecare #headlines #music #beat #photooftheday #rap #hiphop #rapper #youngmoney #artist",
                        "#selenagomez #selena #gomez #TagsForLikes #selly #sel #selenamariegomez #TFLers #beautiful #cute #instagood #pretty #gorgeous #hair #selenator #selenators #sellyselena #bestoftheday #selala #love #cutie #pickles #wowp #wizardsofwaverlyplace #beauty",
                        "#mileycyrus #miley #cyrus #TagsForLikes #mileyraycyrus #hannahmontana #hannah #montana #disney #pretty #disney #beautiful #loveher #smile #instagood #instamiley #instacyrus #photooftheday #pop #music #TFLers #breakout #cantbetamed #destiny #smilers #nobodysperfect #actress #singer #destinyhopecyrus #destinycyrus",
                        "#demilovato #demi #lovato #TagsForLikes #demetria #lovatics #lovatic #lovaticforever #ddlovato #devonne #staystrong #beautiful #cute #instagood #pretty #giveyourheartabreak #skyscraper #gorgeous #hair #love #cutie #beauty",
                        "#ariana #arianagrande #grande #TagsForLikes #beautiful #pretty #cute #gorgeous #instagood #instaariana #instagrande #photooftheday #actress #arianator #victorious #redhair #teamariana #catvalentine #caterinavalentine #caterina #cat",
                        "#chrisbrown #chris #brown #TagsForLikes #cb #breezy #chris #rap #rapper #rnb #TFLers #teambreezy #cute #hot #instagood #music #tattoo #photooftheday #singer #dancer #awesome #guy #chrisbreezy",
                        "#rihanna #singer #rihannanavy #TagsForLikes #navy #rihannanavi #rihannafenty #TFLers #rihannadiamonds #instagood #diamonds #unapologetic #riri #photooftheday #music #pop #robyn #fenty #rih #rihnavy #beautiful #pretty #robynfenty",
                        "#austin #mahone #austinmahone #TagsForLikes #TagsForLikesApp #amahone #austinm #mahomie #texas #singer #amazing #talented #instagood #instafollow #love #mahomies #mahomiegram #mahomieforever #saysomething #sayyourejustafriend #whataboutlove #bangabanga #followme",
                        "#5sos #5secondsofsummer #TagsForLikes #luke #lukehemmings #lukey #ashton #ash #ashtonirwin #calum #cal #calumhood #michael #mike #michaelclifford #boyband #music #boys #love #cute #amazing #followme #5sosfans #5sosfamily #5sosfam #TagsForLikesApp #fivesecondsofsummer"
]
let entertaimentTags = [ "#music #genre #song #songs #TagsForLikes #TagsForLikesApp #melody #hiphop #rnb #pop #love #rap #dubstep #instagood #beat #beats #jam #myjam #party #partymusic #newsong #lovethissong #remix #favoritesong #bestsong #photooftheday #bumpin #repeat #listentothis #goodmusic #instamusic",
                         "#movies #theatre #video #TagsForLikes #movie #film #films #videos #actor #actress #cinema #dvd #amc #instamovies #star #moviestar #photooftheday #hollywood #goodmovie #instagood #flick #flicks #instaflick #instaflicks",
                         "#books #book #read #TagsForLikes #reading #reader #page #pages #paper #instagood #kindle #nook #library #author #bestoftheday #bookworm #readinglist #love #photooftheday #imagine #plot #climax #story #literature #literate #stories #words #text",
                         "#videogames #games #gamer #TagsForLikes #gaming #instagaming #instagamer #playinggames #online #photooftheday #onlinegaming #videogameaddict #instagame #instagood #gamestagram #gamerguy #gamergirl #gamin #video #game #igaddict #winning #play #playing"
]
let electronicTags = [ "#electronics #technology #tech #TagsForLikes #electronic #device #gadget #gadgets #instatech #instagood #geek #techie #nerd #techy #photooftheday #computers #laptops #hack #screen",
                       "##iphone #iphoneonly #apple #socialenvy #PleaseForgiveMe #appleiphone #ios #iphone6 #iphone7 #technology #electronics #mobile #instagood #instaiphone #phone #photooftheday #smartphone #iphoneography #iphonegraphy #iphoneographer #iphoneology #iphoneographers #iphonegraphic #iphoneogram #teamiphone",
                       "#android #androidonly #google #socialenvy #PleaseForgiveMe #googleandroid #droid #instandroid #instaandroid #instadroid #instagood #ics #samsung #samsunggalaxys7 #samsunggalaxyedge #samsunggalaxy #phone #smartphone #mobile #androidography #androidographer #androidinstagram #androidnesia #androidcommunity #teamdroid #teamandroid"
]
let followTags = [ "#fslc #followshoutoutlikecomment #TagsForLikesFSLC #TagsForLikesApp #follow #shoutout #followme #comment #TagsForLikes #f4f #s4s #l4l #c4c #followback #shoutoutback #likeback #commentback #love #instagood #photooftheday #pleasefollow #pleaseshoutout #pleaselike #pleasecomment #teamfslcback #fslcback #follows #shoutouts #likes #comments #fslcalways",
                   "#follow #f4f #followme #TagsForLikes #TFLers #TagsForLikesApp #followforfollow #follow4follow #teamfollowback #followher #followbackteam #followhim #followall #followalways #followback #me #love #pleasefollow #follows #follower #following",
                   "#shoutout #shoutouts #shout #out #TagsForLikes #TFLers #TagsForLikesApp #shoutouter #instagood #s4s #shoutoutforshoutout #shoutout4shoutout #so #so4so #photooftheday #ilovemyfollowers #love #sobackteam #soback #follow #f4f #followforfollow #followback #followhim #followher #followall #followme #shout_out",
                   "#followme #like4like #TagsForLikes #TFLers #liker #likes #l4l #likes4likes #photooftheday #love #likeforlike #likesforlikes #liketeam #likeback #likebackteam #instagood #likeall #likealways #TagsForLikesApp #liking",
                   "#comment #comment4comment #TagsForLikes #TFLers #c4c #commenter #comments #commenting #love #comments4comments #instagood #commentteam #commentback #commentbackteam #commentbelow #photooftheday #commentall #commentalways #pleasecomment"
]
let travelTags = [ "#travel #traveling #TagsForLikes #TFLers #vacation #visiting #instatravel #instago #instagood #trip #holiday #photooftheday #fun #travelling #tourism #tourist #instapassport #instatraveling #mytravelgram #travelgram #travelingram #igtravel",
                   "#cars #car #ride #drive #TagsForLikes #driver #sportscar #vehicle #vehicles #street #road #freeway #highway #sportscars #exotic #exoticcar #exoticcars #speed #tire #tires #spoiler #muffler #race #racing #wheel #wheels #rim #rims #engine #horsepower",
                   "#motorcycle #motorcycles #bike #TagsForLikes #ride #rideout #bike #biker #bikergang #helmet #cycle #bikelife #streetbike #cc #instabike #instagood #instamotor #motorbike #photooftheday #instamotorcycle #instamoto #instamotogallery #supermoto #cruisin #cruising #bikestagram",
                   "#skateboarding #skating #skater #TagsForLikes #instaskater #sk8 #sk8er #sk8ing #sk8ordie #photooftheday #board #longboard #longboarding #riding #kickflip #ollie #instagood #wheels #skatephotoaday #skateanddestroy #skateeverydamnday #skatespot #skaterguy #skatergirl #skatepark #skateboard #skatelife",
                   "#health #fitness #fit #TagsForLikes #TFLers #fitnessmodel #fitnessaddict #fitspo #workout #bodybuilding #cardio #gym #train #training #photooftheday #health #healthy #instahealth #healthychoices #active #strong #motivation #instagood #determination #lifestyle #diet #getfit #cleaneating #eatclean #exercise",
                   "#instafit #motivation #fit #TagsForLikes #TFLers #fitness #gymlife #pushpullgrind #grindout #flex #instafitness #gym #trainhard #eatclean #grow #focus #dedication #strength #ripped #swole #fitnessgear #muscle #shredded #squat #bigbench #cardio #sweat #grind #lifestyle #pushpullgrind",
                   "#sports #sport #active #fit #TagsForLikes #football #soccer #basketball #futball #ball #gametime #fun #game #games #crowd #fans #play #playing #player #field #green #grass #score #goal #action #kick #throw #pass #win #winning",
                   "#run #runner #running #TagsForLikes #fit #runtoinspire #furtherfasterstronger #seenonmyrun #trailrunning #trailrunner #runchat #runhappy #instagood #time2run #instafit #happyrunner #marathon #runners #photooftheday #trailrun #fitness #workout #cardio #training #instarunner #instarun #workouttime",
                   "#dance #dancer #dancing #TagsForLikes #dancerecital #music #song #songs #ballet #dancers #dancefloor #danceshoes #instaballet #studio #instadance #instagood #workout #cheer #choreography #flexible #flexibility #photooftheday #love #practice #fun",
                   "#cheer #cheerleading #cheerleader #TagsForLikes #TFLers #cheerathletics #stunt #stunting #tumbling #jump #toetouch #flexible #box #stretch #scale #scorpion #backtuck #instacheer #love #cheerstagram #sport #fit #cheerperfection #cheerclassic #instacheerleader #cheerislife #cheering #cheersport #cheerpassion #cheerpractice",
                   "#gymnastics #gymnastic #gymnast #TagsForLikes #gymnasts #love #fun #flexible #gymnastique #gymnastlife #beam #vault #bars #training #motivation #gym #flip #gym #stunt #sport #git #leap #TagsForLikesApp #scale #highbar #flipping",
                   "#basketball #basket #ball #TagsForLikes #baller #hoop #balling #sports #sport #court #net #rim #backboard #instagood #game #photooftheday #TFLers #active #pass #throw #shoot #instaballer #instaball #jump #nba #bball",
                   "#soccer #ball #futbol #TagsForLikes #futball #kick #pass #shoot #score #goal #field #TFLers #net #team #soccerball #photooftheday #instafutbol #instagood #grass #run #soccergame #fifa #worldcup",
                   "#football #ball #pass #TagsForLikes #footballgame #footballseason #footballgames #footballplayer #instagood #pass #jersey #stadium #field #yards #photooftheday #yardline #pads #touchdown #TFLers #catch #quarterback #fit #grass #nfl #superbowl #kickoff #run",
                   "#baseball #base #ball #TagsForLikes #bases #homerun #bat #throw #catch #swing #photooftheday #field #pitcher #TFLers #mlb #firstbase #game #instagood #secondbase #thirdbase #inning #baseballbat #mitt #gloves #out #sport #sports",
                   "#hockey #hockeystick #puck #TagsForLikes #ice #rink #icerink #hockeyplayer #instagood #hockeyplayers #fight #photooftheday #shot #skate #TFLers #hockeygram #stanleycup #score #hockeylife #pucklife #nhl"
]
let otherTags = [ "#school #class #classess #TagsForLikes #teacher #teachers #student #students #instagood #classmates #classmate #peer #work #homework #bored #books #book #photooftheday #textbook #textbooks #messingaround",
                  "#work #working #job #TagsForLikes #myjob #office #company #bored #grind #mygrind #dayjob #ilovemyjob #dailygrind #photooftheday #business #biz #life #workinglate #computer #instajob #instalife #instagood #instadaily",
                  "#funny #lol #lmao #lmfao #TagsForLikes #hilarious #laugh #laughing #tweegram #fun #friends #photooftheday #friend #wacky #crazy #silly #witty #instahappy #joke #jokes #joking #epic #instagood #instafun #funnypictures #haha #humor",
                  "#quote #quotes #comment #comments #TagsForLikes #TFLers #tweegram #quoteoftheday #song #funny #life #instagood #love #photooftheday #igers #instagramhub #tbt #instadaily #true #instamood #nofilter #word",
                  "#tbt #throwbackthursday #throwbackthursdays #tbts #TagsForLikes #throwback #tb #instatbt #instatb #reminisce #reminiscing #backintheday #photooftheday #back #memories #instamemory #miss #old #instamoment #instagood #throwbackthursdayy #throwbackthursdayyy",
                  "#instagramdirect #instadirect #TagsForLikes #directmessage #direct #message #messageme #TagsForLikesApp #instadirectme",
                  "#tumblr #tumblrlife #tumblrphoto #TagsForLikes #tumblrphotos #tumblrlove #tumblrpic #tumblrpics #tumblrposts #tumblrpost #perfect #tumblrpicture #tumblrpictures #tumblrthings #tumblrstuff #instatumblr #beautiful #love #TagsForLikes #tumblrgram",
                  "#kik #TagsForLikesApp #kikme #kikmessenger #TagsForLikes #TFLers #kikit #kikster #kikmebored #kikmeguys #kikmessanger #boredkikme #kikmeplease #kikmessage #follow #kikmee #kikmemaybe #kikplease #letskik #instakik",
                  "#snapchat #snap #chat #TagsForLikes #snapchatme #TFLers #snapchatmenow #snapchatit #snapchatster #instagood #snapchatmguys #snapchatmegirl #snapchatmeimbored #photooftheday #snapchatmeplease #snapit #snapchatmemaybe #instasnapchat #letssnapchat",
                  "#askfm #ask #askmeanything #TagsForLikes #TFLers #askfmme #questions #question #opinions #anonymous #askme #askmestuff #bored #love #leavequestions #bored #justask #justquestion #askfmit #askmemaybe #askforask #asknow #askaway #askmesomething #spamme #linkinbio",
                  "#money #cash #green #TagsForLikes #dough #bills #crisp #benjamin #benjamins #franklin #franklins #bank #payday #hundreds #twentys #fives #ones #100s #20s #greens #photooftheday #instarich #instagood #capital #stacks #stack #bread #paid",
                  "#colors #color #colorful #TagsForLikes #red #orange #yellow #green #blue #indigo #violet #beautiful #rainbow #rainbowcolors #colour #roygbiv #instacolor #instagood #colorgram #colores #vibrant #multicolor #multicolored #instacolorful #colorworld",
                  "#spiritual #faith #faithful #TagsForLikes #god #grace #pray #prayers #praying #amen #believe #religion #coexist #spirituality #trust #peace #calm #mind #soul #hope #destiny #wisdom #compassion #forgiveness #thankful #knowledge #meditation #life #meditate #guidance"
]

