//
//  SubTagsTableViewController.swift
//  Instagram_API
//
//  Created by User543 on 08.08.17.
//  Copyright © 2017 User543. All rights reserved.
//

import UIKit

class SubTagsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    //MARK: Variables
    
    var subCategories: [String] = []
    var dictionary: [String] = []
    var index = 0
    
    //MARK: IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Remove the drop shadow from the navigation bar
        navigationController!.navigationBar.clipsToBounds = true
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        
        navigationItem.title = "Categories"
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.backgroundView = UIImageView(image: #imageLiteral(resourceName: "GradientBackground"))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subCategories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubTagsViewCell", for: indexPath)
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.text = subCategories[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        index = indexPath.row
        performSegue(withIdentifier: "ToInfoTagsViewController", sender: nil)
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ToInfoTagsViewController" {
            let infoTagsVC: InfoTagsViewController = segue.destination as! InfoTagsViewController
            infoTagsVC.text = dictionary[index]
        }
    }
    
    deinit {
        print("Deinit SubTagsTableViewController ok")
    }
}
