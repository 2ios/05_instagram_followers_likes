//
//  MenuItemCell.swift
//  Instagram_API
//
//  Created by Andrei Golban on 7/29/17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import UIKit

class MenuItemCell: UITableViewCell {
    
    @IBOutlet weak var menuItemImageView: UIImageView!
    
    func configureForMenuItem(menuItem: NSDictionary) {
        menuItemImageView.image = UIImage(named: menuItem["image"] as! String)
        backgroundColor = UIColor.clear
    }
}
