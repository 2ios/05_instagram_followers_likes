//
//  InstagramUser.m
//  FollowMeter
//
//  Created by Motipan Serj on 6/27/17.
//  Copyright © 2017 User. All rights reserved.
//

#import "InstagramUser.h"

@implementation InstagramUser

-(id)initWithId:(NSString*)id_user name:(NSString*)name fullName:(NSString*)fullName pictureUrl:(NSString*)pictureURL state:(NSInteger)state isPrivate:(BOOL)isPrivate
{
    self = [self init];
    if (self != nil)
    {
        self.id_user     = id_user;
        self.name       = name;
        self.fullName   = fullName;
        self.pictureURL = pictureURL;
        self.state      = state;
        self.is_private  = isPrivate;
    }
    
    return self;
}
-(id)initWithJSON:(NSDictionary*)json
{
    self = [self init];
    if (self != nil)
    {
        if ([json objectForKey:@"logged_in_user"])
        {
            self.id_user=[[json objectForKey:@"logged_in_user"] objectForKey:@"pk"];
            self.name=[[json objectForKey:@"logged_in_user"] objectForKey:@"username"];
            self.fullName=[[json objectForKey:@"logged_in_user"] objectForKey:@"full_name"];
            self.pictureURL=[[json objectForKey:@"logged_in_user"] objectForKey:@"profile_pic_url"];
            self.state=1;
            self.is_private=[[[json objectForKey:@"logged_in_user"] objectForKey:@"is_private"] boolValue];
            
        }
    }
    
    return self;
}
-(id)init
{
    self = [super init];
    if (self != nil)
    {
        
    }
    
    return self;
}
-(NSDictionary*)encodeUser
{
    return @{@"id_user":self.id_user,
             @"name":self.name,
             @"fullName":self.fullName,
             @"pictureURL":self.pictureURL,
             @"is_private":@(self.is_private)};
}
-(void)encodeWithCoder:(NSCoder *)encoder
{
    //Encode the properties of the object
    [encoder encodeObject:self.id_user forKey:@"id_user"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.fullName forKey:@"fullName"];
    [encoder encodeObject:self.pictureURL forKey:@"pictureURL"];
    [encoder encodeObject:@(self.is_private) forKey:@"is_private"];
    
}

-(id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if ( self != nil )
    {
        //decode the properties
        self.id_user = [decoder decodeObjectForKey:@"id_user"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.fullName = [decoder decodeObjectForKey:@"fullName"];
        self.pictureURL = [decoder decodeObjectForKey:@"pictureURL"];
        self.is_private = [[decoder decodeObjectForKey:@"is_private"] boolValue];
    }
    return self;
}
@end
