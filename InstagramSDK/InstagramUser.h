//
//  InstagramUser.h
//  FollowMeter
//
//  Created by Motipan Serj on 6/27/17.
//  Copyright © 2017 User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstagramUser : NSObject
@property (strong, nonatomic) NSString *id_user;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *fullName;
@property (strong, nonatomic) NSString *pictureURL;
@property (assign, nonatomic) NSInteger state;
@property (assign, nonatomic) BOOL is_private;
-(id)initWithId:(NSString*)id_user name:(NSString*)name fullName:(NSString*)fullName pictureUrl:(NSString*)pictureURL state:(NSInteger)state isPrivate:(BOOL)isPrivate;
-(id)initWithJSON:(NSDictionary*)json;
-(NSDictionary*)encodeUser;
@end
