//
//  InstagramSDK.swift
//  Instagram_API
//
//  Created by Andrei Golban on 21.07.17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import UIKit
import Foundation

class Swift_InstagramSDK {
    
    typealias result = (_ user: InstagramUser?, _ error: Error?) -> Void
    typealias resultLogout = (_ succes: Bool, _ error: Error?) -> Void
    typealias resultProfileInfo = (_ dictionary: NSDictionary?, _ error: Error?) -> Void
    
    static let sharedInstance = Swift_InstagramSDK()
    
    let guid: String? = nil
    let idDevice: String? = nil
    let csrftoken: String? = nil
    let cookies: String? = nil
    
    //MARK: New Headers from server
    func checkNewHeaders() {
        if UserDefaults.standard.object(forKey: "GUID") == nil {
            UserDefaults.standard.set("e1712d2f592becfdea858c4d0ad4e7c5f230c446094155a1663d612e1290c841", forKey: "IG_SIG_KEY")
            UserDefaults.standard.set("d60dfe1d-df0e-42b0-a4e1-81b14c0b76d2", forKey: "GUID")
            UserDefaults.standard.set("4", forKey: "SIG_KEY_VERSION")
            UserDefaults.standard.set([["User-Agent": "Instagram 10.9.0 Android (24/7.0; 640dpi; 1440x2560; HUAWEI; LON-L29; HWLON; hi3660; en_US)"], ["Connection": "keep-alive"], ["Accept": "*/*"], ["Accept-Encoding": "gzip, deflate, sdch"], ["Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"], ["Accept-Language": "en-US"]], forKey: "HEADERS")
        }
        let url = URL(string: "https://instagram.mindscape.xyz/keys")
        var request = URLRequest(url: url!, cachePolicy: .useProtocolCachePolicy , timeoutInterval: 60.0)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "GET"
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        let postDataTask = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("Error checkNewHeadersfunction \(String(describing: error))")
                return
            }
            guard let data = data else {
                print("Data was not recieved checkNewHeadersfunction")
                return
            }
            do {
                guard let requestReply = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
                    print("Error requestReply checkNewHeadersfunction")
                    return
                }
                if requestReply.object(forKey: "GUID") != nil {
                    UserDefaults.standard.set(requestReply.object(forKey: "GUID"), forKey: "GUID")
                    UserDefaults.standard.set(requestReply.object(forKey: "SIG_KEY_VERSION"), forKey: "SIG_KEY_VERSION")
                    UserDefaults.standard.set(requestReply.object(forKey: "IG_SIG_KEY"), forKey: "IG_SIG_KEY")
                    let arr = requestReply.object(forKey: "HEADERS") as! NSArray
                    let mut = NSMutableArray()
                    for i in 0..<arr.count {
                        mut.add(NSDictionary(dictionary: [(arr.value(forKey: "key") as! NSArray).object(at: i) : (arr.value(forKey: "value") as! NSArray).object(at: i)]))
                    }
                    UserDefaults.standard.set(mut, forKey: "HEADERS")
                }
            } catch let error as NSError {
                print("Error catch checkNewHeadersfunction \(error.localizedDescription)")
            }
        }
        postDataTask.resume()
    }
    
    // MARK: Request Create
    func getSignedBody(dict: NSDictionary) -> NSData? {
        do {
            let data = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
            var string = String(data: data, encoding: .utf8)!
            string = string.replacingOccurrences(of: " ", with: "")
            string = string.replacingOccurrences(of: "\n", with: "")
            let signedHMAC = string.hmacsha256String(withKey: UserDefaults.standard.object(forKey: "IG_SIG_KEY") as! String!)!
            let string2 = "ig_sig_key_version=\(UserDefaults.standard.object(forKey: "SIG_KEY_VERSION")!)&signed_body=\(signedHMAC).\(string)"
            return NSData(data: string2.data(using: .utf8)!)
        } catch let error as NSError {
            print("Error catch getSignedBodyfunction \(error.localizedDescription)")
            return nil
        }
    }
    
    func requestWithURL(url: URL, body: NSData?, method: String, username: String) -> URLRequest {
        var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
        if (UserDefaults.standard.object(forKey: "accountsUsers") != nil) {
            let dict = UserDefaults.standard.object(forKey: "accountsUsers") as! NSMutableDictionary
            if dict.object(forKey: username) != nil {
                if ((dict.object(forKey: username) as! NSMutableDictionary).object(forKey: "userCookies") != nil) {
                    request.addValue((dict.object(forKey: username) as! NSMutableDictionary).object(forKey: "userCookies") as! String, forHTTPHeaderField: "Cookie")
                }
            }
        }
        let arrayHeaders = UserDefaults.standard.object(forKey: "HEADERS") as! NSArray
        for case let dict as NSDictionary in arrayHeaders {
            for case let key as String in dict.allKeys {
                request.addValue(dict.value(forKey: key) as! String, forHTTPHeaderField: key)
            }
        }
        request.httpMethod = method
        if (body != nil) {
            request.httpBody = body as Data?
        }
        return request
    }
    
    
    //==========================================================================
    //MARK: Class Instagram
    func login(email: String, password: String, completion: @escaping result) -> Void {
        let paramDictionary = NSDictionary(dictionary: ["username" : email, "guid" : UserDefaults.standard.object(forKey: "GUID")!, "device_id" : UIDevice.current.identifierForVendor!.uuidString, "password" : password, "login_attempt_count" : "0"])
        let data = self.getSignedBody(dict: paramDictionary)
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        let url = Swift_RequestConstants.sharedInstance.login()
        let request = self.requestWithURL(url: url, body: data, method: "POST", username: email)
        let postDataTask = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("Error loginWithEmailfunction \(String(describing: error))")
                return
            }
            guard let data = data else {
                print("Data was not recieved loginWithEmailfunction")
                DispatchQueue.main.async(execute: {() -> Void in
                    completion(nil, error)
                })
                return
            }
            do {
                guard let requestReply = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
                    print("Error requestReply loginWithEmailfunction")
                    return
                }
                if (requestReply.object(forKey: "status") != nil) {
                    if requestReply.object(forKey: "status") as! String == "ok" {
                        var str = ""
                        var dictss = ""
                        for case let cookie: HTTPCookie in HTTPCookieStorage.shared.cookies! {
                            str = "\(str)\(cookie.name)=\(cookie.value); "
                            if cookie.name == "csrftoken" {
                                dictss = cookie.value
                            }
                        }
                        let user: InstagramUser = InstagramUser(json: requestReply as! [AnyHashable : Any])
                        let myEncodedObject = NSKeyedArchiver.archivedData(withRootObject: user)
                        if UserDefaults.standard.object(forKey: "accountsUsers") as? String == nil {
                            let dict = NSMutableDictionary()
                            dict.setObject(NSDictionary(dictionary: ["userObject" : myEncodedObject, "userCookies" : str, "csrftoken" : dictss]), forKey: user.name as NSCopying)
                            UserDefaults.standard.set(dict, forKey: "accountsUsers")
                            self.changeSelectedUser(username: user.name!)
                        } else {
                            let dict = (UserDefaults.standard.object(forKey: "accountsUsers") as! NSMutableDictionary).mutableCopy()
                            (dict as! NSMutableDictionary).setObject(NSDictionary(dictionary: ["userObject": myEncodedObject, "userCookies" : str, "csrftoken" : dictss]), forKey: "accountsUsers" as NSCopying)
                            self.changeSelectedUser(username: user.name!)
                        }
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(user, nil)
                        })
                    } else {
                        let errMsg = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: requestReply.object(forKey: "message")!]) as? [AnyHashable : Any])
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(nil, errMsg)
                        })
                    }
                } else {
                    DispatchQueue.main.async(execute: {() -> Void in
                        completion(nil, error)
                    })
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        postDataTask.resume()
    }
    
    func deleteFromDocuments(id_user: String) -> Void {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        do {
            let directoryContent = try FileManager.default.contentsOfDirectory(atPath: documentsDirectory)
            for string in directoryContent {
                if string.contains("\(id_user)") {
                    let plistPath: String = URL(fileURLWithPath: documentsDirectory).appendingPathComponent(string).absoluteString
                    do {
                        try FileManager.default.removeItem(atPath: plistPath)
                    } catch let error as NSError {
                        print(error.description)
                    }
                }
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    func logout(username: String, completion: @escaping resultLogout) -> Void {
        if (UserDefaults.standard.object(forKey: "accountsUsers") != nil) {
            let dict = (UserDefaults.standard.object(forKey: "accountsUsers") as! NSMutableDictionary).mutableCopy() as! NSMutableDictionary
            let data8 = (dict.value(forKey: username) as! NSMutableDictionary).value(forKey: "userObject") as! Data
            let user: InstagramUser = NSKeyedUnarchiver.unarchiveObject(with: data8) as! InstagramUser
            let paramDictionary = NSDictionary(dictionary: ["device_id" : UIDevice.current.identifierForVendor!.uuidString, "_csrftoken" : (dict.value(forKey: username) as! NSMutableDictionary).value(forKey: "csrftoken")!, "one_tap_app_login": "true"])
            let data = self.getSignedBody(dict: paramDictionary)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
            let url = Swift_RequestConstants.sharedInstance.logout()
            let request = self.requestWithURL(url: url, body: data, method: "POST", username: username)
            let postDataTask = session.dataTask(with: request) { (data, response, error) in
                guard error == nil else {
                    print("Error logoutCurentUserfunction \(String(describing: error))")
                    return
                }
                guard let data = data else {
                    print("Data was not recieved logoutCurentUserfunction")
                    DispatchQueue.main.async(execute: {() -> Void in
                        completion(true, error)
                    })
                    return
                }
                do {
                    guard let requestReply = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
                        print("Error requestReply logoutCurentUserfunction")
                        return
                    }
                    if (requestReply.object(forKey: "status") != nil) {
                        if ((requestReply.object(forKey: "status") as! String) == "ok") {
                            if (dict.object(forKey: username) != nil) {
                                dict.removeObject(forKey: username)
                            }
                            self.deleteFromDocuments(id_user: String(user.encode()[AnyHashable("id_user")] as! Int64))
                            UserDefaults.standard.set(dict, forKey: "accountsUsers")
                            if dict.count > 0 {
                                let array = dict.allKeys as NSArray
                                self.changeSelectedUser(username: array.firstObject as! String)
                                DispatchQueue.main.async(execute: {() -> Void in
                                    completion(false, nil)
                                })
                            } else {
                                UserDefaults.standard.set(nil, forKey: "curentUser")
                                DispatchQueue.main.async(execute: {() -> Void in
                                    completion(true, nil)
                                })
                            }
                        } else {
                            let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: requestReply.object(forKey: "message")!]) as? [AnyHashable : Any])
                            DispatchQueue.main.async(execute: {() -> Void in
                                completion(true, errMsg)
                            })
                        }
                    } else {
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(true, error)
                        })
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            postDataTask.resume()
        }
    }
    
    func changeSelectedUser(username: String) -> Void {
        UserDefaults.standard.set(username, forKey: "curentUser")
    }
    
    //==========================================================================
    //MARK: Class Media
    func getMediaInfo(media_id: String, username: String, completion: @escaping resultProfileInfo) -> Void {
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        let url = Swift_RequestConstants.sharedInstance.getMediaInfo(media_id: media_id)
        let request = self.requestWithURL(url: url, body: nil, method: "GET", username: username)
        let postDataTask = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("Error getMediaInfofunction \(String(describing: error))")
                return
            }
            guard let data = data else {
                print("Data was not recieved getMediaInfofunction")
                DispatchQueue.main.async(execute: {() -> Void in
                    completion(nil, error)
                })
                return
            }
            do {
                guard let requestReply = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
                    print("Error requestReply getMediaInfofunction")
                    return
                }
                if (requestReply.object(forKey: "status") != nil) {
                    if requestReply.object(forKey: "status") as! String == "ok" {
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, error)
                        })
                    } else {
                        let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: requestReply.object(forKey: "message")!]) as? [AnyHashable : Any])
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, errMsg)
                        })
                    }
                } else {
                    let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: "error"]) as? [AnyHashable : Any])
                    DispatchQueue.main.async(execute: {() -> Void in
                        completion(requestReply, errMsg)
                    })
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        postDataTask.resume()
    }
    
    func getMediaSavedFeed(username: String, next_id: String, completion: @escaping resultProfileInfo) -> Void {
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        let url = Swift_RequestConstants.sharedInstance.getMediaSavedFeed(next_id: next_id)
        let request = self.requestWithURL(url: url, body: nil, method: "GET", username: username)
        let postDataTask = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("Error getMediaSavedfunction \(String(describing: error))")
                return
            }
            guard let data = data else {
                print("Data was not recieved getMediaSavedfunction")
                DispatchQueue.main.async(execute: {() -> Void in
                    completion(nil, error)
                })
                return
            }
            do {
                guard let requestReply = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
                    print("Error requestReply getMediaSavedfunction")
                    return
                }
                if (requestReply.object(forKey: "status") != nil) {
                    if requestReply.object(forKey: "status") as! String == "ok" {
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, error)
                        })
                    } else {
                        let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: requestReply.object(forKey: "message")!]) as? [AnyHashable : Any])
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, errMsg)
                        })
                    }
                } else {
                    let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: "error"]) as? [AnyHashable : Any])
                    DispatchQueue.main.async(execute: {() -> Void in
                        completion(requestReply, errMsg)
                    })
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        postDataTask.resume()
    }
    
    func getLikers(media_id: String, username: String, next_id: String, completion: @escaping resultProfileInfo) -> Void {
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        let url = Swift_RequestConstants.sharedInstance.getLikers(media_id: media_id, next_id: next_id)
        let request = self.requestWithURL(url: url, body: nil, method: "GET", username: username)
        let postDataTask = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("Error getLikersMediafunction \(String(describing: error))")
                return
            }
            guard let data = data else {
                print("Data was not recieved getLikersMediafunction")
                DispatchQueue.main.async(execute: {() -> Void in
                    completion(nil, error)
                })
                return
            }
            do {
                guard let requestReply = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
                    print("Error requestReply getLikersMediafunction")
                    return
                }
                if (requestReply.object(forKey: "status") != nil) {
                    if requestReply.object(forKey: "status") as! String == "ok" {
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, error)
                        })
                    } else {
                        let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: requestReply.object(forKey: "message")!]) as? [AnyHashable : Any])
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, errMsg)
                        })
                    }
                } else {
                    let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: "error"]) as? [AnyHashable : Any])
                    DispatchQueue.main.async(execute: {() -> Void in
                        completion(requestReply, errMsg)
                    })
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        postDataTask.resume()
    }
    
    func getLikedFeed(username: String, next_id: String, completion: @escaping resultProfileInfo) -> Void {
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        let url = Swift_RequestConstants.sharedInstance.getLikedFeed(next_id: next_id)
        let request = self.requestWithURL(url: url, body: nil, method: "GET", username: username)
        let postDataTask = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("Error getLikedMediafunction \(String(describing: error))")
                return
            }
            guard let data = data else {
                print("Data was not recieved getLikedMediafunction")
                DispatchQueue.main.async(execute: {() -> Void in
                    completion(nil, error)
                })
                return
            }
            do {
                guard let requestReply = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
                    print("Error requestReply getLikedMediafunction")
                    return
                }
                if (requestReply.object(forKey: "status") != nil) {
                    if requestReply.object(forKey: "status") as! String == "ok" {
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, error)
                        })
                    } else {
                        let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: requestReply.object(forKey: "message")!]) as? [AnyHashable : Any])
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, errMsg)
                        })
                    }
                } else {
                    let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: "error"]) as? [AnyHashable : Any])
                    DispatchQueue.main.async(execute: {() -> Void in
                        completion(requestReply, errMsg)
                    })
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        postDataTask.resume()
    }
    
    func getFeedMedia(username: String, max_next_id: String, completion: @escaping resultProfileInfo) -> Void {
        let dict = (UserDefaults.standard.object(forKey: "accountsUsers") as! NSMutableDictionary).mutableCopy() as! NSMutableDictionary
        let data = (dict.value(forKey: username) as! NSMutableDictionary).value(forKey: "userObject") as! Data
        let user: InstagramUser = (NSKeyedUnarchiver.unarchiveObject(with: data) as! InstagramUser)
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        let url = Swift_RequestConstants.sharedInstance.getFeedMedia(user_id: "\(user.encode()[AnyHashable("id_user")]!)", nextId: max_next_id)
        let request = self.requestWithURL(url: url, body: nil, method: "GET", username: username)
        let postDataTask = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("Error getFeedMediafunction \(String(describing: error))")
                return
            }
            guard let data = data else {
                print("Data was not recieved getFeedMediafunction")
                DispatchQueue.main.async(execute: {() -> Void in
                    completion(nil, error)
                })
                return
            }
            do {
                guard let requestReply = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
                    print("Error requestReply getFeedMediafunction")
                    return
                }
                if (requestReply.object(forKey: "status") != nil) {
                    if requestReply.object(forKey: "status") as! String == "ok" {
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, error)
                        })
                    } else {
                        let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: requestReply.object(forKey: "message")!]) as? [AnyHashable : Any])
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, errMsg)
                        })
                    }
                } else {
                    let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: "error"]) as? [AnyHashable : Any])
                    DispatchQueue.main.async(execute: {() -> Void in
                        completion(requestReply, errMsg)
                    })
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        postDataTask.resume()
    }
    
    //=============================================================================
    //MARK: Class People
    func getInfoByName(username: String, completion: @escaping resultProfileInfo) -> Void {
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        let url = Swift_RequestConstants.sharedInstance.getInfoByName(username)
        let request = self.requestWithURL(url: url, body: nil, method: "GET", username: username)
        let postDataTask = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("Error getInfoByNamefunction \(String(describing: error))")
                return
            }
            guard let data = data else {
                print("Data was not recieved getInfoByNamefunction")
                DispatchQueue.main.async(execute: {() -> Void in
                    completion(nil, error)
                })
                return
            }
            do {
                guard let requestReply = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
                    print("Error requestReply getInfoByNamefunction")
                    return
                }
                if (requestReply.object(forKey: "status") != nil) {
                    if requestReply.object(forKey: "status") as! String == "ok" {
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, error)
                        })
                    } else {
                        let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: requestReply.object(forKey: "message")!]) as? [AnyHashable : Any])
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, errMsg)
                        })
                    }
                } else {
                    let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: "error"]) as? [AnyHashable : Any])
                    DispatchQueue.main.async(execute: {() -> Void in
                        completion(requestReply, errMsg)
                    })
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        postDataTask.resume()
    }
    
    func getInfoById(username: String, completion: @escaping resultProfileInfo) -> Void {
        let dict = (UserDefaults.standard.object(forKey: "accountsUsers") as! NSMutableDictionary).mutableCopy() as! NSMutableDictionary
        let data = (dict.value(forKey: username) as! NSMutableDictionary).value(forKey: "userObject") as! Data
        let user: InstagramUser = (NSKeyedUnarchiver.unarchiveObject(with: data) as! InstagramUser)
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        let url = Swift_RequestConstants.sharedInstance.getInfoById("\(user.encode()[AnyHashable("id_user")]!)")
        let request = self.requestWithURL(url: url, body: nil, method: "GET", username: username)
        let postDataTask = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("Error getProfileInfofunction \(String(describing: error))")
                return
            }
            guard let data = data else {
                print("Data was not recieved getProfileInfofunction")
                DispatchQueue.main.async(execute: {() -> Void in
                    completion(nil, error)
                })
                return
            }
            do {
                guard let requestReply = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
                    print("Error requestReply getProfileInfofunction")
                    return
                }
                if (requestReply.object(forKey: "status") != nil) {
                    if requestReply.object(forKey: "status") as! String == "ok" {
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply.object(forKey: "user") as! NSDictionary?, error)
                        })
                    } else {
                        let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: requestReply.object(forKey: "message")!]) as? [AnyHashable : Any])
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, errMsg)
                        })
                    }
                } else {
                    let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: "error"]) as? [AnyHashable : Any])
                    DispatchQueue.main.async(execute: {() -> Void in
                        completion(requestReply, errMsg)
                    })
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        postDataTask.resume()
    }
    
    func getRecentActivityInbox(username: String, completion: @escaping resultProfileInfo) -> Void {
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        let url = Swift_RequestConstants.sharedInstance.getRecentActivityInbox(username)
        let request = self.requestWithURL(url: url, body: nil, method: "GET", username: username)
        let postDataTask = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("Error getRecentActivityInboxfunction \(String(describing: error))")
                return
            }
            guard let data = data else {
                print("Data was not recieved getRecentActivityInboxfunction")
                DispatchQueue.main.async(execute: {() -> Void in
                    completion(nil, error)
                })
                return
            }
            do {
                guard let requestReply = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
                    print("Error requestReply getRecentActivityInboxfunction")
                    return
                }
                if (requestReply.object(forKey: "status") != nil) {
                    if requestReply.object(forKey: "status") as! String == "ok" {
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, error)
                        })
                    } else {
                        let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: requestReply.object(forKey: "message")!]) as? [AnyHashable : Any])
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, errMsg)
                        })
                    }
                } else {
                    let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: "error"]) as? [AnyHashable : Any])
                    DispatchQueue.main.async(execute: {() -> Void in
                        completion(requestReply, errMsg)
                    })
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        postDataTask.resume()
    }
    
    func getFollowingRecentActivity(username: String, next_id: String, completion: @escaping resultProfileInfo) -> Void {
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        let url = Swift_RequestConstants.sharedInstance.getFollowingRecentActivity(username, next_id: next_id)
        let request = self.requestWithURL(url: url, body: nil, method: "GET", username: username)
        let postDataTask = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("Error getFollowingRecentActivityfunction \(String(describing: error))")
                return
            }
            guard let data = data else {
                print("Data was not recieved getFollowingRecentActivityfunction")
                DispatchQueue.main.async(execute: {() -> Void in
                    completion(nil, error)
                })
                return
            }
            do {
                guard let requestReply = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
                    print("Error requestReply getFollowingRecentActivityfunction")
                    return
                }
                if (requestReply.object(forKey: "status") != nil) {
                    if requestReply.object(forKey: "status") as! String == "ok" {
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, error)
                        })
                    } else {
                        let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: requestReply.object(forKey: "message")!]) as? [AnyHashable : Any])
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, errMsg)
                        })
                    }
                } else {
                    let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: "error"]) as? [AnyHashable : Any])
                    DispatchQueue.main.async(execute: {() -> Void in
                        completion(requestReply, errMsg)
                    })
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        postDataTask.resume()
    }
    
    func getFriendship(username: String, user_id: String, completion: @escaping resultProfileInfo) -> Void {
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        let url = Swift_RequestConstants.sharedInstance.getFriendship(user_id)
        let request = self.requestWithURL(url: url, body: nil, method: "GET", username: username)
        let postDataTask = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("Error getFriendshipfunction \(String(describing: error))")
                return
            }
            guard let data = data else {
                print("Data was not recieved getFriendshipfunction")
                DispatchQueue.main.async(execute: {() -> Void in
                    completion(nil, error)
                })
                return
            }
            do {
                guard let requestReply = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
                    print("Error requestReply getFriendshipfunction")
                    return
                }
                if (requestReply.object(forKey: "status") != nil) {
                    if requestReply.object(forKey: "status") as! String == "ok" {
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, error)
                        })
                    } else {
                        let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: requestReply.object(forKey: "message")!]) as? [AnyHashable : Any])
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, errMsg)
                        })
                    }
                } else {
                    let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: "error"]) as? [AnyHashable : Any])
                    DispatchQueue.main.async(execute: {() -> Void in
                        completion(requestReply, errMsg)
                    })
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        postDataTask.resume()
    }
    
    // MARK: Nu lucreaza cum trebuie functia data
    func getFriendships(username: String, userIDs: NSArray, completion: @escaping resultProfileInfo) -> Void {
        if (UserDefaults.standard.object(forKey: "accountsUsers") != nil) {
            let dict = (UserDefaults.standard.object(forKey: "accountsUsers") as! NSMutableDictionary).mutableCopy() as! NSMutableDictionary
            let stringUserUDs = userIDs.componentsJoined(by: ",")
            let paramDictionary = NSDictionary(dictionary: ["_uuid" : UIDevice.current.identifierForVendor!.uuidString, "_csrftoken" : (dict.value(forKey: username) as! NSMutableDictionary).value(forKey: "csrftoken")!, "user_ids": stringUserUDs])
            let data = self.getSignedBody(dict: paramDictionary)
            let configuration = URLSessionConfiguration.default
            let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
            let url = Swift_RequestConstants.sharedInstance.getFriendships()
            let request = self.requestWithURL(url: url, body: data, method: "POST", username: username)
            let postDataTask = session.dataTask(with: request) { (data, response, error) in
                guard error == nil else {
                    print("Error getFriendshipsfunction \(String(describing: error))")
                    return
                }
                guard let data = data else {
                    print("Data was not recieved getFriendshipsfunction")
                    DispatchQueue.main.async(execute: {() -> Void in
                        completion(nil, error)
                    })
                    return
                }
                do {
                    guard let requestReply = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
                        print("Error requestReply getFriendshipsfunction")
                        return
                    }
                    if (requestReply.object(forKey: "status") as? NSDictionary != nil) {
                        if ((requestReply.object(forKey: "status") as! NSDictionary).object(forKey: "ok") as? NSDictionary != nil) {
                            completion(requestReply, nil)
                        } else {
                            let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: requestReply.object(forKey: "message")!]) as? [AnyHashable : Any])
                            DispatchQueue.main.async(execute: {() -> Void in
                                completion(requestReply, errMsg)
                            })
                        }
                    } else {
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, error)
                        })
                    }
                } catch let error as NSError {
                    print(error.localizedDescription)
                }
            }
            postDataTask.resume()
        }
    }
    
    func getPendingFriendships(username: String, completion: @escaping resultProfileInfo) -> Void {
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        let url = Swift_RequestConstants.sharedInstance.getPendingFriendships()
        let request = self.requestWithURL(url: url, body: nil, method: "GET", username: username)
        let postDataTask = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("Error getPendingFriendshipsfunction \(String(describing: error))")
                return
            }
            guard let data = data else {
                print("Data was not recieved getFriendshipsfunction")
                DispatchQueue.main.async(execute: {() -> Void in
                    completion(nil, error)
                })
                return
            }
            do {
                guard let requestReply = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
                    print("Error requestReply getFriendshipsfunction")
                    return
                }
                if (requestReply.object(forKey: "status") != nil) {
                    if requestReply.object(forKey: "status") as! String == "ok" {
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, error)
                        })
                    } else {
                        let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: requestReply.object(forKey: "message")!]) as? [AnyHashable : Any])
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, errMsg)
                        })
                    }
                } else {
                    let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: "error"]) as? [AnyHashable : Any])
                    DispatchQueue.main.async(execute: {() -> Void in
                        completion(requestReply, errMsg)
                    })
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        postDataTask.resume()
    }
    
    func getFollowers(username: String, next_id: String, completion: @escaping resultProfileInfo) -> Void {
        let dict = (UserDefaults.standard.object(forKey: "accountsUsers") as! NSMutableDictionary).mutableCopy() as! NSMutableDictionary
        let data = (dict.value(forKey: username) as! NSMutableDictionary).value(forKey: "userObject") as! Data
        let user: InstagramUser = (NSKeyedUnarchiver.unarchiveObject(with: data) as! InstagramUser)
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        let url = Swift_RequestConstants.sharedInstance.getFollowers(user_id: "\(user.encode()[AnyHashable("id_user")]!)", next_id: next_id)
        let request = self.requestWithURL(url: url, body: nil, method: "GET", username: username)
        let postDataTask = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("Error getFollowersListfunction \(String(describing: error))")
                return
            }
            guard let data = data else {
                print("Data was not recieved getFollowersListfunction")
                DispatchQueue.main.async(execute: {() -> Void in
                    completion(nil, error)
                })
                return
            }
            do {
                guard let requestReply = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
                    print("Error requestReply getFollowersListfunction")
                    return
                }
                if (requestReply.object(forKey: "status") != nil) {
                    if requestReply.object(forKey: "status") as! String == "ok" {
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, error)
                        })
                    } else {
                        let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: requestReply.object(forKey: "message")!]) as? [AnyHashable : Any])
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, errMsg)
                        })
                    }
                } else {
                    let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: "error"]) as? [AnyHashable : Any])
                    DispatchQueue.main.async(execute: {() -> Void in
                        completion(requestReply, errMsg)
                    })
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        postDataTask.resume()
    }
    
    func getFollowing(username: String, next_id: String, completion: @escaping resultProfileInfo) -> Void {
        let dict = (UserDefaults.standard.object(forKey: "accountsUsers") as! NSMutableDictionary).mutableCopy() as! NSMutableDictionary
        let data = (dict.value(forKey: username) as! NSMutableDictionary).value(forKey: "userObject") as! Data
        let user: InstagramUser = (NSKeyedUnarchiver.unarchiveObject(with: data) as! InstagramUser)
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        let url = Swift_RequestConstants.sharedInstance.getFollowing(user_id: "\(user.encode()[AnyHashable("id_user")]!)", next_id: next_id)
        let request = self.requestWithURL(url: url, body: nil, method: "GET", username: username)
        let postDataTask = session.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                print("Error getFollowingListfunction \(String(describing: error))")
                return
            }
            guard let data = data else {
                print("Data was not recieved getFollowingListfunction")
                DispatchQueue.main.async(execute: {() -> Void in
                    completion(nil, error)
                })
                return
            }
            do {
                guard let requestReply = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary else {
                    print("Error requestReply getFollowingListfunction")
                    return
                }
                if (requestReply.object(forKey: "status") != nil) {
                    if requestReply.object(forKey: "status") as! String == "ok" {
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, error)
                        })
                    } else {
                        let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: requestReply.object(forKey: "message")!]) as? [AnyHashable : Any])
                        DispatchQueue.main.async(execute: {() -> Void in
                            completion(requestReply, errMsg)
                        })
                    }
                } else {
                    let errMsg: NSError = NSError.init(domain: "domain", code: 1001, userInfo: NSDictionary(dictionary: [NSLocalizedDescriptionKey: "error"]) as? [AnyHashable : Any])
                    DispatchQueue.main.async(execute: {() -> Void in
                        completion(requestReply, errMsg)
                    })
                }
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        postDataTask.resume()
    }
}




