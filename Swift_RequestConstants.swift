//
//  RequestConstants.swift
//  Instagram_API
//
//  Created by Andrei Golban on 21.07.17.
//  Copyright © 2017 Andrei Golban. All rights reserved.
//

import Foundation

class Swift_RequestConstants {
    
    let host: String = "https://i.instagram.com/api/v1/"
    
    static let sharedInstance = Swift_RequestConstants()
    
    //==========================================================================
    //MARK: Class Instagram
    //Login to Instagram
    func login() -> URL {
        return URL(string: "\(host)accounts/login/")!
    }
    
    //Log out of Instagram.
    func logout() -> URL {
        return URL(string: "\(host)accounts/logout/")!
    }
    
    
    //==========================================================================
    //MARK: Class Account
    //MARK: Functie neutilizata
    //Sets your account to private.
    func setPrivateURL() -> URL {
        return URL(string: "\(host)accounts/set_private/")!
    }
    
    //Sets your account to public.
    //MARK: Functie neutilizata
    func setPublicURL() -> URL {
        return URL(string: "\(host)accounts/set_public/")!
    }
    
    //==========================================================================
    //MARK: Class Media
    //Get detailed media information.
    func getMediaInfo(media_id: String) -> URL {
        return URL(string: "\(host)media/\(media_id)/info/")!
    }
    
    //MARK: Functie neutilizata
    //Like a media item.
    func likeMediaItem(_ media_id: String) -> URL {
        return URL(string: "\(host)media/\(media_id)/like/")!
    }
    
    //Unlike a media item.
    //MARK: Functie neutilizata
    func unlikeMediaItem(_ media_id: String) -> URL {
        return URL(string: "\(host)media/\(media_id)/unlike/")!
    }
    
    //Get saved media items feed.
    func getMediaSavedFeed(next_id: String) -> URL {
        if next_id != "" {
            return URL(string: "\(host)feed/saved/?max_id=\(next_id)")!
        }
        return URL(string: "\(host)feed/saved/")!
    }
    
    //Get list of users who liked a media item.
    //The media ID in Instagram's internal format (ie "3482384834_43294").
    func getLikers(media_id: String, next_id: String) -> URL {
        if next_id != "" {
            return URL(string: "\(host)media/\(media_id)/likers/?max_id=\(next_id)")!
        }
        return URL(string: "\(host)media/\(media_id)/likers/")!
    }
    
    //Get feed of your liked media.
    func getLikedFeed(next_id: String) -> URL {
        if next_id != "" {
            return URL(string: "\(host)feed/liked/?max_id=\(next_id)")!
        }
        return URL(string: "\(host)feed/liked/")!
    }
    
    func getFeedMedia(user_id: String, nextId: String) -> URL {
        if nextId != "" {
            return URL(string: "\(host)feed/user/\(user_id)/?max_id=\(nextId)")!
        }
        return URL(string: "\(host)feed/user/\(user_id)/")!
    }
    
    //=============================================================================
    //MARK: Class Usertag
    //MARK: Functie neutilizata
    //Get user taggings for a user.
    func getUserFeed(_ user_id: String) -> URL {
        return URL(string: "\(host)usertags/\(user_id)/feed/")!
    }
    
    //=============================================================================
    //MARK: Class People
    //Get details about a specific user via their username
    func getInfoByName(_ username: String) -> URL {
        return URL(string: "\(host)users/\(username)/usernameinfo/")!
    }
    
    //Get details about a specific user via their numerical UserPK ID.
    func getInfoById(_ user_id: String) -> URL {
        return URL(string: "\(host)users/\(user_id)/info/")!
    }
    
    /*Get other people's recent activities related to you and your posts.
     *
     * This feed has information about when people interact with you, such as
     * liking your posts, commenting on your posts, tagging you in photos or in
     * comments, people who started following you, etc. */
    func getRecentActivityInbox(_ username: String) -> URL {
        return URL(string: "\(host)news/inbox/")!
    }
    
    /* Get news feed with recent activities by accounts you follow.
     *
     * This feed has information about the people you follow, such as what posts
     * they've liked or that they've started following other people. */
    func getFollowingRecentActivity(_ username: String, next_id: String) -> URL {
        if next_id != "" {
            return URL(string: "\(host)news/?max_id=\(next_id)")!
        }
        return URL(string: "\(host)news/")!
    }
    
    //Show a user's friendship status with you.
    func getFriendship(_ user_id: String) -> URL {
        return URL(string: "\(host)friendships/show/\(user_id)/")!
    }
    
    //Show multiple users' friendship status with you.
    func getFriendships() -> URL {
        return URL(string: "\(host)friendships/show_many/")!
    }
    
    //Get list of pending friendship requests.
    func getPendingFriendships() -> URL {
        return URL(string: "\(host)friendships/pending/")!
    }
    
    //Get list of who a user is followed by.
    func getFollowers(user_id: String, next_id: String) -> URL {
        if next_id != "" {
            return URL(string: "\(host)friendships/\(user_id)/followers/?max_id=\(next_id)")!
        }
        return URL(string: "\(host)friendships/\(user_id)/followers/")!
    }
    //Get list of who a user is following.
    func getFollowing(user_id: String, next_id: String) -> URL {
        if next_id != "" {
            return URL(string: "\(host)friendships/\(user_id)/following/?max_id=\(next_id)")!
        }
        return URL(string: "\(host)friendships/\(user_id)/following")!
    }
}
